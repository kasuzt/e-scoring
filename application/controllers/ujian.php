<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ujian extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "peserta") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");
			
            $no_peserta = $this->session->userdata('username');

            $d['cek_ujian'] = $this->db->query("SELECT * FROM tbl_ujian WHERE no_peserta=$no_peserta AND waktu_selesai is not null");
            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE no_peserta = $no_peserta AND no_peserta NOT IN (SELECT no_peserta FROM tbl_ujian)");
            $d['data_berkas'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_berkas.* FROM tbl_data_peserta, tbl_berkas WHERE tbl_data_peserta.no_peserta = tbl_berkas.no_peserta");
            $d['main'] = 'ujian/ujian_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function mulai_ujian() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "peserta") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $no_peserta = decrypt_url($this->uri->segment(3));

            $query = $this->db->get_where('tbl_ujian', array('no_peserta' => $no_peserta));
            if ($query->num_rows() == 1) {
                $id['no_peserta'] = decrypt_url($this->uri->segment(3));
                $this->session->set_userdata($id);
                $data_peserta = $this->db->get_where("tbl_data_peserta", $id);

                $query1 = $this->db->query("SELECT tbl_master_ujian.*, tbl_user_login.* FROM tbl_master_ujian, tbl_user_login WHERE tbl_master_ujian.id_master_ujian = tbl_user_login.id_master_ujian AND tbl_user_login.username = $no_peserta");

                foreach ($query1->result() as $data) {
                    $d['judul'] = $data->judul;
                    $d['keterangan'] = $data->keterangan;
                    $d['file'] = $data->file;
                }

                if ($data_peserta->num_rows() > 0) {
                    $q = $this->db->get_where("tbl_data_peserta", $id);
                    $set_detail = $q->row();
                    $this->session->set_userdata("nama_ktp", $set_detail->nama_ktp);

                    foreach ($q->result() as $data) {
                        $d['id_peserta'] = $data->id_peserta;
                        $d['nama_ktp'] = $data->nama_ktp;
                        $d['nik'] = $data->nik;
                        $d['no_peserta'] = $data->no_peserta;
                        $d['jenis_kelamin'] = $data->jenis_kelamin;
                        $d['jabatan_nama'] = $data->jabatan_nama;
                        $d['jenis_formasi'] = $data->jenis_formasi;
                        $d['satker'] = $data->satker;
                        $d['lembaga_pendidikan'] = $data->lembaga_pendidikan;
                        $d['id_praktek_kerja'] = $data->id_praktek_kerja;
                    }
                    $d['main'] = 'ujian/ujian_hadir';
                    $this->load->vars($d);
                    $this->load->view('template/template');
                } else {
                    redirect(base_url());
                }
            } else {
                $in['id_master_ujian'] = $this->session->userdata('id_master_ujian');
                $in['no_peserta'] = decrypt_url($this->uri->segment(3));
                $in['waktu_mulai'] = date('Y-m-d H:i:s');
                $in['create_time'] = date('Y-m-d H:i:s');
                $this->db->insert("tbl_ujian", $in);
                redirect(base_url("ujian/mulai_ujian/" . encrypt_url($this->session->userdata('username'))));
            }
        } else {
            redirect(base_url());
        }
    }

    public function selesai_ujian() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "peserta") {
            $id['no_peserta'] = decrypt_url($this->uri->segment(3));

            $upd['waktu_selesai'] = date('Y-m-d H:i:s');
            $upd['update_time'] = date('Y-m-d H:i:s');
            $this->db->update("tbl_ujian", $upd, $id);
            redirect(base_url("ujian"));
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function upload() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "peserta") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $id['no_peserta'] = decrypt_url($this->uri->segment(3));
            $this->session->set_userdata($id);
            $data_peserta = $this->db->get_where("tbl_data_peserta", $id);

            if ($data_peserta->num_rows() > 0) {
                $q = $this->db->get_where("tbl_data_peserta", $id);
                $set_detail = $q->row();
                $this->session->set_userdata("nama_ktp", $set_detail->nama_ktp);

                foreach ($q->result() as $data) {
                    $d['id_peserta'] = $data->id_peserta;
                    $d['nama_ktp'] = $data->nama_ktp;
                    $d['nik'] = $data->nik;
                    $d['no_peserta'] = $data->no_peserta;
                    $d['jenis_kelamin'] = $data->jenis_kelamin;
                    $d['jabatan_nama'] = $data->jabatan_nama;
                    $d['jenis_formasi'] = $data->jenis_formasi;
                    $d['satker'] = $data->satker;
                    $d['lembaga_pendidikan'] = $data->lembaga_pendidikan;
                }

                $d['main'] = 'ujian/ujian_upload';
                $this->load->vars($d);
                $this->load->view('template/template');
            } else {
                header('location:' . base_url() . '');
            }
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function upload_simpan() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "peserta") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $config['upload_path'] = './upload/ujian/';
            $config['allowed_types'] = 'doc|docx|xls|xlsx|ppt|pptx';
            $config['max_size'] = 3000;
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config);
            $keterangan_berkas = $this->input->post('keterangan_berkas');
            $jumlah_berkas = count($_FILES['berkas']['name']);

            $statusUpload = [];

            for ($i = 0; $i < $jumlah_berkas; $i++) {
                if (!empty($_FILES['berkas']['name'][$i])) {

                    $_FILES['file']['name'] = $_FILES['berkas']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['berkas']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['berkas']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['berkas']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['berkas']['size'][$i];

                    if ($this->upload->do_upload('file')) {

                        $uploadData = $this->upload->data();
                        $data['nama_berkas'] = $uploadData['file_name'];
                        $data['tipe_berkas'] = $uploadData['file_ext'];
                        $data['ukuran_berkas'] = $uploadData['file_size'];
                        $data['keterangan_berkas'] = $keterangan_berkas[$i];
                        $data['no_peserta'] = addslashes($this->input->post('no_peserta'));
                        $data['waktu_upload'] = date('Y-m-d H:i:s');
                        $this->db->insert('tbl_berkas', $data);
                        $statusUpload[$i]['nama'] = $_FILES['berkas']['name'][$i];
                        $statusUpload[$i]['status'] = "berhasil";
                    } else {
                        $statusUpload[$i]['nama'] = $_FILES['berkas']['name'][$i];
                        $statusUpload[$i]['status'] = "gagal";
                    }
                }
            }
			redirect('ujian/upload');
        } else {
            redirect(base_url());
        }
    }

    function download($id) {
        $data = $this->db->get_where('tbl_berkas', ['kd_berkas' => $id])->row();
        force_download('uploads/ujian/' . $data->nama_berkas, "berkas");
    }

}

/* End of file ujian.php */
/* Location: ./application/controllers/ujian.php */