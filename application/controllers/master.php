<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Master extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_master_ujian");
            $config['base_url'] = base_url() . 'peserta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['master_ujian'] = $this->db->query("SELECT * FROM tbl_master_ujian LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'master_ujian/home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function cari() {
        if (($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator")) {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_master_ujian WHERE judul LIKE '%" . $kata . "%' ORDER BY nama_ktp");
            $config['base_url'] = base_url() . 'peserta/cari/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['master_ujian'] = $this->db->query("SELECT * FROM tbl_master_ujian WHERE judul LIKE '%" . $kata . "%' LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'master_ujian/home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function tambah() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['id_param'] = "";
            $d['nama_eselon'] = "";
            $d['st'] = "tambah";

            $d['main'] = 'master_ujian/input';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function detail() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $id['id_master_ujian'] = $this->uri->segment(3);
            $q = $this->db->get_where("tbl_master_ujian", $id);
            $d = array();
            foreach ($q->result() as $dt) {
                $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
                $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
                $d['instansi'] = $this->config->item('nama_instansi');
                $d['credit'] = $this->config->item('credit_aplikasi');
                $d['alamat'] = $this->config->item('alamat_instansi');

                $d['id_param'] = $dt->id_master_ujian;
                $d['judul'] = $dt->judul;
                $d['keterangan'] = $dt->keterangan;
                $d['file'] = $dt->file;
                $d['create_time'] = $dt->create_time;
                $d['update_time'] = $dt->update_time;
            }
            $d['st'] = "edit";

            $d['main'] = 'master_ujian/detail';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function edit() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $id['id_master_ujian'] = $this->uri->segment(3);
            $q = $this->db->get_where("tbl_master_ujian", $id);
            $d = array();
            foreach ($q->result() as $dt) {
                $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
                $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
                $d['instansi'] = $this->config->item('nama_instansi');
                $d['credit'] = $this->config->item('credit_aplikasi');
                $d['alamat'] = $this->config->item('alamat_instansi');

                $d['id_param'] = $dt->id_master_ujian;
                $d['judul'] = $dt->judul;
                $d['keterangan'] = $dt->keterangan;
                $d['file'] = $dt->file;
            }
            $d['st'] = "edit";

            $d['main'] = 'master_ujian/input';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function simpan() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $this->form_validation->set_rules('judul', 'Nama Ujian', 'trim|required');
            $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
            $id['id_master_ujian'] = $this->input->post("id_param");
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            if ($this->form_validation->run() == FALSE) {
                $st = $this->input->post('st');
                if ($st == "edit") {
                    $q = $this->db->get_where("tbl_master_ujian", $id);
                    $d = array();
                    foreach ($q->result() as $dt) {
                        $d['id_param'] = $dt->id_master_ujian;
                        $d['nama_eselon'] = $dt->nama_eselon;
                    }
                    $d['st'] = $st;

                    $d['main'] = 'master_ujian/input';
                    $this->load->vars($d);
                    $this->load->view('template/template');
                } else if ($st == "tambah") {
                    $d['id_param'] = "";
                    $d['judul'] = "";
                    $d['keterangan'] = "";
                    $d['userfile'] = "";
                    $d['st'] = $st;

                    $d['main'] = 'master_ujian/input';
                    $this->load->vars($d);
                    $this->load->view('template/template');
                }
            } else {
                $st = $this->input->post('st');
                if ($st == "edit") {
                    $upd['judul'] = $this->input->post("judul");
                    $upd['keterangan'] = $this->input->post("keterangan");

                    if (empty($_FILES['userfile']['name'])) {
                        $now = date("Y-m-d H:i:s");
                        $upd['judul'] = $this->input->post("judul");
                        $upd['keterangan'] = $this->input->post("keterangan");
                        $upd['update_time'] = $now;

                        $this->db->update("tbl_master_ujian", $in_data, $upd);
                        $this->session->set_flashdata('message', 'Master Ujian Berhasil Diupdate');
                        redirect("master");
                    } else {
                        $config['upload_path'] = './upload/master_ujian/';
                        $config['allowed_types'] = 'pdf';
                        $config['encrypt_name'] = true;
                        $config['remove_spaces'] = true;
                        $config['max_size'] = '3000';

                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload("userfile")) {
                            $now = date("Y-m-d H:i:s");
                            $data = $this->upload->data();
                            $in_data['file'] = $data['file_name'];
                            $in_data['update_time'] = $now;
                            $this->db->update("tbl_master_ujian", $in_data, $id);

                            $this->session->set_flashdata('message', 'Master Ujian Berhasil Diupdate');
                            redirect("master");
                        } else {
                            echo $this->upload->display_errors('<p>', '</p>');
                        }
                    }

                    $this->db->update("tbl_master_ujian", $upd, $id);
                    redirect('master', 'refresh');
                } else if ($st == "tambah") {
                    $now = date("Y-m-d H:i:s");
                    if (empty($_FILES['userfile']['name'])) {
                        $in_data['judul'] = $this->input->post('judul');
                        $in_data['keterangan'] = $this->input->post('keterangan');
                        $in_data['create_time'] = $now;
                        $this->db->insert("tbl_master_ujian", $in_data);

                        $this->session->set_flashdata('berhasil', 'Master Ujian Berhasil Disimpan');
                        redirect("master/index");
                    } else {
                        $config['upload_path'] = './upload/master_ujian/';
                        $config['allowed_types'] = 'pdf';
                        $config['encrypt_name'] = true;
                        $config['remove_spaces'] = true;
                        $config['max_size'] = '3000';

                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload("userfile")) {
                            $data = $this->upload->data();

                            // Permission Configuration
                            chmod($source, 0777);

                            $in_data['judul'] = $this->input->post('judul');
                            $in_data['keterangan'] = $this->input->post('keterangan');
                            $in_data['create_time'] = $now;
                            $in_data['file'] = $data['file_name'];

                            $this->db->insert("tbl_master_ujian", $in_data);

                            $this->session->set_flashdata('berhasil', 'Master Ujian Berhasil Disimpan');
                            redirect("master/index");
                        }
                    }
                }
            }
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function hapus() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $id['id_master_ujian'] = $this->uri->segment(3);
            $this->db->delete("tbl_master_ujian", $id);
            header('location:' . base_url() . 'master_eselon');
        } else {
            header('location:' . base_url() . '');
        }
    }

}

/* End of file master.php */
/* Location: ./application/controllers/master.php */