<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Peserta extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta");
            $config['base_url'] = base_url() . 'peserta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'peserta/peserta_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function cari() {
        if (($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator")) {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp");
            $config['base_url'] = base_url() . 'peserta/cari/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'peserta/peserta_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function tambah_pengguna() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $in['username'] = $this->input->post("username");
            $in['nama_lengkap'] = $this->input->post("nama_lengkap");
            $in['kode_satker'] = $this->input->post("kode_satker");
            $in['jabatan'] = $this->input->post("jabatan_nama");
            $in['nip'] = $this->input->post("nik");
            $in['stts'] = 'peserta';
            $in['password'] = md5($this->input->post("password") . 'AppSimpeg32');
            $this->db->insert("tbl_user_login", $in);
            redirect('peserta/pengadministrasi', 'refresh');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function detail() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $id['id_peserta'] = $this->uri->segment(3);
            $q = $this->db->get_where("tbl_data_peserta", $id);
            $d = array();
            foreach ($q->result() as $dt) {
                $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
                $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
                $d['instansi'] = $this->config->item('nama_instansi');
                $d['credit'] = $this->config->item('credit_aplikasi');
                $d['alamat'] = $this->config->item('alamat_instansi');

                $d['id_param'] = $dt->id_peserta;
                $d['nama_ktp'] = $dt->nama_ktp;
                $d['no_peserta'] = $dt->no_peserta;
                $d['nik'] = $dt->nik;
                $d['no_register'] = $dt->no_register;
                $d['tempat_lahir_ktp'] = $dt->tempat_lahir_ktp;
                $d['tgl_lahir_ktp'] = $dt->tgl_lahir_ktp;
                $d['jenis_kelamin'] = $dt->jenis_kelamin;
                $d['alamat_domisili'] = $dt->alamat_domisili;
                $d['lembaga_pendidikan'] = $dt->lembaga_pendidikan;
                $d['ipk_nilai'] = $dt->ipk_nilai;
                $d['jabatan_nama'] = $dt->jabatan_nama;
                $d['jenis_formasi'] = $dt->jenis_formasi;
                $d['satker'] = $dt->satker;
                $d['twk'] = $dt->twk;
                $d['tiu'] = $dt->tiu;
                $d['tkp'] = $dt->tkp;
                $d['total'] = $dt->total;
            }
            $d['st'] = "edit";

            $d['main'] = 'peserta/peserta_detail';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

}

/* End of file peserta.php */
/* Location: ./application/controllers/peserta.php */