<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nilai extends CI_Controller {

    public function beladiri() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator" || $this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $page = $this->uri->segment(4);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_beladiri.* FROM tbl_data_peserta, tbl_nilai_beladiri WHERE tbl_data_peserta.id_peserta = tbl_nilai_beladiri.id_peserta");
            $config['base_url'] = base_url() . 'nilai/beladiri/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_beladiri.* FROM tbl_data_peserta, tbl_nilai_beladiri WHERE tbl_data_peserta.id_peserta = tbl_nilai_beladiri.id_peserta ORDER BY created_time desc LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'nilai/beladiri';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function beladiri_cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator" || $this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(4);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;
            $d['tot'] = $offset;

            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_beladiri.* FROM tbl_data_peserta, tbl_nilai_beladiri WHERE tbl_data_peserta.id_peserta = tbl_nilai_beladiri.id_peserta AND tbl_data_peserta.nama_ktp LIKE '%" . $kata . "%' OR tbl_data_peserta.no_peserta LIKE '%" . $kata . "%'");
            $config['base_url'] = base_url() . 'nilai/beladiri/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_beladiri.* FROM tbl_data_peserta, tbl_nilai_beladiri WHERE tbl_data_peserta.id_peserta = tbl_nilai_beladiri.id_peserta AND tbl_data_peserta.nama_ktp LIKE '%" . $kata . "%' OR tbl_data_peserta.no_peserta LIKE '%" . $kata . "%' ORDER BY created_time desc LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'nilai/beladiri';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function praktek_kerja() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $page = $this->uri->segment(4);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_praktek_kerja.* FROM tbl_data_peserta, tbl_nilai_praktek_kerja WHERE tbl_data_peserta.id_peserta = tbl_nilai_praktek_kerja.id_peserta");
            $config['base_url'] = base_url() . 'nilai/praktek_kerja/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_praktek_kerja.* FROM tbl_data_peserta, tbl_nilai_praktek_kerja WHERE tbl_data_peserta.id_peserta = tbl_nilai_praktek_kerja.id_peserta ORDER BY created_time desc LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'nilai/praktek_kerja';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function praktek_kerja_cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(4);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;
            $d['tot'] = $offset;

            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_praktek_kerja.* FROM tbl_data_peserta, tbl_nilai_praktek_kerja WHERE tbl_data_peserta.id_peserta = tbl_nilai_praktek_kerja.id_peserta AND tbl_data_peserta.nama_ktp LIKE '%" . $kata . "%' OR tbl_data_peserta.no_peserta LIKE '%" . $kata . "%'");
            $config['base_url'] = base_url() . 'nilai/praktek_kerja/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_praktek_kerja.* FROM tbl_data_peserta, tbl_nilai_praktek_kerja WHERE tbl_data_peserta.id_peserta = tbl_nilai_praktek_kerja.id_peserta AND tbl_data_peserta.nama_ktp LIKE '%" . $kata . "%' OR tbl_data_peserta.no_peserta LIKE '%" . $kata . "%' ORDER BY created_time desc LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'nilai/praktek_kerja';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function samapta() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator" || $this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $page = $this->uri->segment(4);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_samapta_koni.* FROM tbl_data_peserta, tbl_nilai_samapta_koni WHERE tbl_data_peserta.id_peserta = tbl_nilai_samapta_koni.id_peserta");
            $config['base_url'] = base_url() . 'nilai/samapta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_samapta_koni.* FROM tbl_data_peserta, tbl_nilai_samapta_koni WHERE tbl_data_peserta.id_peserta = tbl_nilai_samapta_koni.id_peserta ORDER BY created_time desc LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'nilai/samapta';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function samapta_cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator" || $this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(4);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;
            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_samapta.* FROM tbl_data_peserta, tbl_nilai_samapta WHERE tbl_data_peserta.id_peserta = tbl_nilai_samapta.id_peserta AND tbl_data_peserta.nama_ktp LIKE '%" . $kata . "%' OR tbl_data_peserta.no_peserta LIKE '%" . $kata . "%'");
            $config['base_url'] = base_url() . 'nilai/samapta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_samapta.* FROM tbl_data_peserta, tbl_nilai_samapta WHERE tbl_data_peserta.id_peserta = tbl_nilai_samapta.id_peserta AND tbl_data_peserta.nama_ktp LIKE '%" . $kata . "%' OR tbl_data_peserta.no_peserta LIKE '%" . $kata . "%' ORDER BY created_time desc LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'nilai/samapta';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function wawancara() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $page = $this->uri->segment(4);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_wawancara.* FROM tbl_data_peserta, tbl_nilai_wawancara WHERE tbl_data_peserta.id_peserta = tbl_nilai_wawancara.id_peserta");
            $config['base_url'] = base_url() . 'nilai/wawancara/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_wawancara.* FROM tbl_data_peserta, tbl_nilai_wawancara WHERE tbl_data_peserta.id_peserta = tbl_nilai_wawancara.id_peserta ORDER BY created_time desc LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'nilai/wawancara';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function wawancara_cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(4);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;
            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_wawancara.* FROM tbl_data_peserta, tbl_nilai_wawancara WHERE tbl_data_peserta.id_peserta = tbl_nilai_wawancara.id_peserta AND tbl_data_peserta.nama_ktp LIKE '%" . $kata . "%' OR tbl_data_peserta.no_peserta LIKE '%" . $kata . "%'");
            $config['base_url'] = base_url() . 'nilai/wawancara/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_wawancara.* FROM tbl_data_peserta, tbl_nilai_wawancara WHERE tbl_data_peserta.id_peserta = tbl_nilai_wawancara.id_peserta AND tbl_data_peserta.nama_ktp LIKE '%" . $kata . "%' OR tbl_data_peserta.no_peserta LIKE '%" . $kata . "%' ORDER BY created_time desc LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'nilai/wawancara';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

}

/* End of file nilai.php */
/* Location: ./application/controllers/nilai.php */