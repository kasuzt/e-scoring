<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_Admin extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['main'] = 'dashboard_admin/home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function panitia_pusat() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['main'] = 'dashboard_admin/home_panitia_pusat';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function pewawancara() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "pewawancara") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['main'] = 'dashboard_admin/home_pewawancara';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function presentasi() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "presentasi") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['main'] = 'dashboard_admin/home_presentasi';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function panitia_daerah() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['main'] = 'dashboard_admin/home_panitia_daerah';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function peserta() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "peserta") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['main'] = 'dashboard_admin/home_peserta';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function koni_pusat() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['main'] = 'dashboard_admin/home_koni_pusat';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

}

/* End of file dashboard_admin.php */
/* Location: ./application/controllers/dashboard_admin.php */