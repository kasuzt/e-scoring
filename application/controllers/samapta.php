<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Samapta extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta) AND pendidikan_nama = 'SLTA/SMA SEDERAJAT'");
            $config['base_url'] = base_url() . 'samapta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta) AND pendidikan_nama = 'SLTA/SMA SEDERAJAT' LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'samapta/samapta_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta) AND jabatan_nama = 'PENGEMUDI PENGAWAL TAHANAN' AND kode_satker = '$kode_satker' AND tgl_samapta = '" . $tgl_sekarang . "'");
            $config['base_url'] = base_url() . 'samapta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta) AND kode_satker = '$kode_satker' AND jabatan_nama = 'PENGEMUDI PENGAWAL TAHANAN' AND tgl_samapta = '" . $tgl_sekarang . "' ORDER BY nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'samapta/samapta_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_samapta.* FROM tbl_data_peserta, tbl_nilai_samapta WHERE tbl_nilai_samapta.id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta_koni) AND tbl_data_peserta.id_peserta = tbl_nilai_samapta.id_peserta");
            $config['base_url'] = base_url() . 'samapta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_samapta.* FROM tbl_data_peserta, tbl_nilai_samapta WHERE tbl_nilai_samapta.id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta_koni) AND tbl_data_peserta.id_peserta = tbl_nilai_samapta.id_peserta LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'samapta/samapta_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta) AND tgl_samapta = '" . $tgl_sekarang . "' AND nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp");
            $config['base_url'] = base_url() . 'samapta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta) AND tgl_samapta = '" . $tgl_sekarang . "' AND nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'samapta/samapta_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta) AND kode_satker = '$kode_satker' AND tgl_samapta = '" . $tgl_sekarang . "' AND nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp");
            $config['base_url'] = base_url() . 'samapta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta) AND kode_satker = '$kode_satker' AND tgl_samapta = '" . $tgl_sekarang . "' AND nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'samapta/samapta_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_samapta.* FROM tbl_data_peserta, tbl_nilai_samapta WHERE tbl_nilai_samapta.id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta_koni) AND tbl_data_peserta.id_peserta = tbl_nilai_samapta.id_peserta AND nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp");
            $config['base_url'] = base_url() . 'samapta/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_samapta.* FROM tbl_data_peserta, tbl_nilai_samapta WHERE tbl_nilai_samapta.id_peserta NOT IN( SELECT id_peserta FROM tbl_nilai_samapta_koni) AND tbl_data_peserta.id_peserta = tbl_nilai_samapta.id_peserta AND nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'samapta/samapta_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function hadir() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator" || $this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $id['id_peserta'] = decrypt_url($this->uri->segment(3));
            $this->session->set_userdata($id);
            $data_peserta = $this->db->get_where("tbl_data_peserta", $id);

            $q = $this->db->get_where("tbl_data_peserta", $id);
            $set_detail = $q->row();
            $this->session->set_userdata("nama_ktp", $set_detail->nama_ktp);

            foreach ($q->result() as $data) {
                $d['id_peserta'] = $data->id_peserta;
                $d['nama_ktp'] = $data->nama_ktp;
                $d['nik'] = $data->nik;
                $d['no_peserta'] = $data->no_peserta;
                $d['jenis_kelamin'] = $data->jenis_kelamin;
                $d['jabatan_nama'] = $data->jabatan_nama;
                $d['jenis_formasi'] = $data->jenis_formasi;
                $d['satker'] = $data->satker;
                $d['lembaga_pendidikan'] = $data->lembaga_pendidikan;
            }

            $d['main'] = 'samapta/samapta_hadir';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $id['id_peserta'] = decrypt_url($this->uri->segment(3));
            $this->session->set_userdata($id);
            $data_peserta = $this->db->get_where("tbl_data_peserta", $id);

            // load data peserta
            $q = $this->db->get_where("tbl_data_peserta", $id);
            $set_detail = $q->row();
            $this->session->set_userdata("nama_ktp", $set_detail->nama_ktp);

            foreach ($q->result() as $data) {
                $d['id_peserta'] = $data->id_peserta;
                $d['nama_ktp'] = $data->nama_ktp;
                $d['nik'] = $data->nik;
                $d['no_peserta'] = $data->no_peserta;
                $d['jenis_kelamin'] = $data->jenis_kelamin;
                $d['jabatan_nama'] = $data->jabatan_nama;
                $d['jenis_formasi'] = $data->jenis_formasi;
                $d['satker'] = $data->satker;
                $d['lembaga_pendidikan'] = $data->lembaga_pendidikan;
            }

            // load data nilai panitia_daerah			
            $q1 = $this->db->get_where("tbl_nilai_samapta", $id);
            $set_detail = $q->row();

            foreach ($q1->result() as $data) {
                $d['lari'] = $data->lari;
                $d['pull_ups'] = $data->pull_ups;
                $d['sit_ups'] = $data->sit_ups;
                $d['push_ups'] = $data->push_ups;
                $d['shuttle_run'] = $data->shuttle_run;
                $d['penguji'] = $data->penguji;
                $d['created_time'] = $data->created_time;
            }

            $d['main'] = 'samapta/samapta_hadir_koni';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function tidak_hadir() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator" || $this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $in['id_peserta'] = decrypt_url($this->uri->segment(3));
            $in['keterangan'] = "TIDAK HADIR TES KESAMAPTAAN";
            $in['penguji'] = $this->session->userdata('username');
            $in['created_time'] = date('Y-m-d H:i:s');

            $this->db->insert("tbl_nilai_samapta", $in);
            header('location:' . base_url() . 'samapta/index/');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function tidak_hadir_koni() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator" || $this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $in['id_peserta'] = decrypt_url($this->uri->segment(3));
            $in['keterangan'] = "TIDAK HADIR TES KESAMAPTAAN";
            $in['penguji'] = $this->session->userdata('username');
            $in['created_time'] = date('Y-m-d H:i:s');

            $this->db->insert("tbl_nilai_samapta_koni", $in);
            header('location:' . base_url() . 'samapta/index/');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function simpan_nilai() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator" || $this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $id_peserta = $this->input->post("id_peserta");
            $query = $this->db->get_where('tbl_nilai_samapta', array('id_peserta' => $id_peserta));
            if ($query->num_rows() == 0) {
                $in['id_peserta'] = addslashes($this->input->post("id_peserta"));
                $in['lari'] = addslashes($this->input->post('lari'));
                $in['pull_ups'] = addslashes($this->input->post('pull_ups'));
                $in['sit_ups'] = addslashes($this->input->post('sit_ups'));
                $in['push_ups'] = addslashes($this->input->post('push_ups'));
                $in['shuttle_run'] = addslashes($this->input->post('shuttle_run'));
                $in['penguji'] = $this->session->userdata('username');
                $in['created_time'] = date('Y-m-d H:i:s');

                $this->db->insert("tbl_nilai_samapta", $in);
                redirect('samapta/index', 'refresh');
            } else {
                redirect('samapta/index', 'refresh');
            }
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function simpan_nilai_koni() {
        if ($this->session->userdata('stts') == "koni_pusat") {
            $id_peserta = $this->input->post("id_peserta");
            $query = $this->db->get_where('tbl_nilai_samapta_koni', array('id_peserta' => $id_peserta));
            if ($query->num_rows() == 0) {
                $in['id_peserta'] = addslashes($this->input->post("id_peserta"));
                $in['lari'] = addslashes($this->input->post('lari_'));
                $in['pull_ups'] = addslashes($this->input->post('pull_ups_'));
                $in['sit_ups'] = addslashes($this->input->post('sit_ups_'));
                $in['push_ups'] = addslashes($this->input->post('push_ups_'));
                $in['shuttle_run'] = addslashes($this->input->post('shuttle_run_'));
                $in['penguji'] = $this->session->userdata('username');
                $in['created_time'] = date('Y-m-d H:i:s');

                $this->db->insert("tbl_nilai_samapta_koni", $in);
                redirect('samapta/index', 'refresh');
            } else {
                redirect('samapta/index', 'refresh');
            }
        } else {
            header('location:' . base_url() . '');
        }
    }

}

/* End of file samapta.php */
/* Location: ./application/controllers/samapta.php */