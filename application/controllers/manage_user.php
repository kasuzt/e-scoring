<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class manage_user extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_user_login");
            $config['base_url'] = base_url() . 'manage_user/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['status_pegawai'] = $this->db->query("SELECT tbl_satker.id_satker, tbl_satker.kode_satker, tbl_satker.satker, tbl_user_login.id_user_login, tbl_user_login.username, tbl_user_login.`password`, tbl_user_login.nama_lengkap, tbl_user_login.stts, tbl_user_login.kode_satker FROM tbl_user_login JOIN tbl_satker ON tbl_satker.kode_satker = tbl_user_login.kode_satker ORDER BY tbl_user_login.id_user_login LIMIT " . $offset . ", " . $limit . "");

            $d['main'] = 'user/list_user';
            $this->load->vars($d);
            $this->load->view('template/template');
        }
        else {
            header('location:' . base_url() . '');
        }
    }

    public function cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_satker.id_satker, tbl_satker.kode_satker, tbl_satker.satker, tbl_user_login.id_user_login, tbl_user_login.username, tbl_user_login.`password`, tbl_user_login.nama_lengkap, tbl_user_login.stts, tbl_user_login.kode_satker FROM tbl_user_login JOIN tbl_satker ON tbl_satker.kode_satker = tbl_user_login.kode_satker WHERE nama_lengkap LIKE '%" . $kata . "%'");
            $config['base_url'] = base_url() . 'manage_user/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['status_pegawai'] = $this->db->query("SELECT tbl_satker.id_satker, tbl_satker.kode_satker, tbl_satker.satker, tbl_user_login.id_user_login, tbl_user_login.username, tbl_user_login.`password`, tbl_user_login.nama_lengkap, tbl_user_login.stts, tbl_user_login.kode_satker FROM tbl_user_login JOIN tbl_satker ON tbl_satker.kode_satker = tbl_user_login.kode_satker WHERE nama_lengkap LIKE '%" . $kata . "%' ORDER BY tbl_user_login.id_user_login LIMIT " . $offset . ", " . $limit . "");

            $d['main'] = 'user/list_user';
            $this->load->vars($d);
            $this->load->view('template/template');
        }
        else {
            header('location:' . base_url() . '');
        }
    }

    public function edit() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $id['id_user_login'] = $this->uri->segment(3);
            $q = $this->db->get_where("tbl_user_login", $id);
            $d = array();
            foreach ($q->result() as $dt) {
                $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
                $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
                $d['instansi'] = $this->config->item('nama_instansi');
                $d['credit'] = $this->config->item('credit_aplikasi');
                $d['alamat'] = $this->config->item('alamat_instansi');
                $d['id_param'] = $dt->id_user_login;
                $d['username'] = $dt->username;
                $d['password'] = $dt->password;
                $d['nama_lengkap'] = $dt->nama_lengkap;
                $d['kode_satker'] = $dt->kode_satker;
                $d['stts'] = $dt->stts;
            }
            $d['st'] = "edit";

            $d['main'] = 'user/input';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function detail() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $id['id_user_login'] = $this->uri->segment(3);
            $q = $this->db->get_where("tbl_user_login", $id);
            $d = array();
            foreach ($q->result() as $dt) {
                $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
                $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
                $d['instansi'] = $this->config->item('nama_instansi');
                $d['credit'] = $this->config->item('credit_aplikasi');
                $d['alamat'] = $this->config->item('alamat_instansi');
                $d['id_param'] = $dt->id_user_login;
                $d['username'] = $dt->username;
                $d['password'] = $dt->password;
                $d['nama_lengkap'] = $dt->nama_lengkap;
                $d['kode_satker'] = $dt->kode_satker;
                $d['stts'] = $dt->stts;
            }
            $d['st'] = "edit";

            $d['main'] = 'user/detail';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function tambah() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');
            $d['id_param'] = "";
            $d['username'] = "";
            $d['password'] = "";
            $d['nama_lengkap'] = "";
            $d['kode_satker'] = "";
            $d['stts'] = "";
            $d['st'] = "tambah";

            $d['main'] = 'user/input';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function hapus() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $id['id_user_login'] = $this->uri->segment(3);
            $this->db->delete("tbl_user_login", $id);
            header('location:' . base_url() . 'manage_user');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function simpan() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
            $id['id_user_login'] = $this->input->post("id_param");
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            if ($this->form_validation->run() == FALSE) {
                $st = $this->input->post('st');
                if ($st == "edit") {
                    $id['id_user_login'] = $this->uri->segment(3);
                    $q = $this->db->get_where("tbl_user_login", $id);
                    $d = array();
                    foreach ($q->result() as $dt) {
                        $d['id_param'] = $dt->id_user_login;
                        $d['username'] = $dt->username;
                        $d['password'] = $dt->password;
                        $d['nama_lengkap'] = $dt->nama_lengkap;
                        $d['kode_satker'] = $dt->kode_satker;
                        $d['stts'] = $dt->stts;
                    }
                    $d['st'] = "edit";

                    $d['main'] = 'user/input';
                    $this->load->vars($d);
                    $this->load->view('template/template');
                } else if ($st == "tambah") {
                    $d['id_param'] = "";
                    $d['username'] = "";
                    $d['password'] = "";
                    $d['nama_lengkap'] = "";
                    $d['kode_satker'] = "";
                    $d['stts'] = "";
                    $d['st'] = "tambah";

                    $d['main'] = 'user/input';
                    $this->load->vars($d);
                    $this->load->view('template/template');
                }
            } else {
                $st = $this->input->post('st');
                if ($st == "edit") {
                    $upd['username'] = $this->input->post("username");
                    $upd['nama_lengkap'] = $this->input->post("nama_lengkap");
                    $upd['kode_satker'] = $this->input->post("kode_satker");
                    if ($this->input->post("password") != "") {
                        $upd['password'] = md5($this->input->post("password") . 'AppSimpeg32');
                    }
                    $this->db->update("tbl_user_login", $upd, $id);
                    redirect('manage_user/index', 'refresh');
                } else if ($st == "tambah") {
                    $login['username'] = $this->input->post("username");
                    $cek = $this->db->get_where('tbl_user_login', $login);
                    if ($cek->num_rows() > 0) {
                        $d['id_param'] = "";
                        $d['username'] = "";
                        $d['password'] = "";
                        $d['nama_lengkap'] = "";
                        $d['kode_satker'] = "";
                        $d['stts'] = "";
                        $d['st'] = "tambah";
                        ?>
                        <script type="text/javascript">
                            alert("Username telah ada, silahkan gunakan yang lainnya...");
                        </script>
                        <?php

                        $d['main'] = 'user/input';
                        $this->load->vars($d);
                        $this->load->view('template/template');
                    } else {
                        $in['username'] = $this->input->post("username");
                        $in['nama_lengkap'] = $this->input->post("nama_lengkap");
                        $in['kode_satker'] = $this->input->post("kode_satker");
                        $in['stts'] = $this->input->post("stts");
                        $in['password'] = md5($this->input->post("password") . 'AppSimpeg32');
                        $this->db->insert("tbl_user_login", $in);
                        redirect('manage_user/index', 'refresh');
                    }
                }
            }
        } else {
            header('location:' . base_url() . '');
        }
    }

}

/* End of file manage_user.php */
/* Location: ./application/controllers/manage_user.php */