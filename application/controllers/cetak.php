<?php

Class Cetak extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['user'] = $this->app_login_model->getAllUser();
            $d['user_wawancara'] = $this->app_login_model->getAllUserPewawancara();

            $d['main'] = 'cetak/home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function komputer() {
        $user = $this->input->post('user');
        $d['data_peserta'] = $this->app_login_model->getCetakKomputer($user);
        $id['username'] = $this->input->post('user');
        $user = $this->input->post('user');
        $q = $this->db->get_where("tbl_user_login", $id);
        $set_detail = $q->row();
        foreach ($q->result() as $data) {
            $d['id_user_login'] = $data->id_user_login;
            $d['username'] = $data->username;
            $d['nama_lengkap'] = $data->nama_lengkap;
            $d['nip'] = $data->nip;
            $d['jabatan'] = $data->jabatan;
        }
        ob_start();
        $content = $this->load->view('cetak/komputer_print', $d);
        $content = ob_get_clean();
        $this->load->library('html2pdf');
        try {
            $html2pdf = new HTML2PDF('L', 'A3', 'en');
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output('Hasil Tes Komputer.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    public function beladiri() {
        $user = $this->input->post('user');
        $d['data_peserta'] = $this->app_login_model->getCetakBeladiri($user);
        $id['username'] = $this->input->post('user');
        $user = $this->input->post('user');
        $q = $this->db->get_where("tbl_user_login", $id);
        $set_detail = $q->row();
        foreach ($q->result() as $data) {
            $d['id_user_login'] = $data->id_user_login;
            $d['username'] = $data->username;
            $d['nama_lengkap'] = $data->nama_lengkap;
            $d['nip'] = $data->nip;
            $d['jabatan'] = $data->jabatan;
        }
        ob_start();
        $content = $this->load->view('cetak/beladiri_print', $d);
        $content = ob_get_clean();
        $this->load->library('html2pdf');
        try {
            $html2pdf = new HTML2PDF('L', 'A3', 'en');
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output('Hasil Tes Beladiri.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    public function mengemudi() {
        $user = $this->input->post('user');
        $d['data_peserta'] = $this->app_login_model->getCetakMengemudi($user);
        $id['username'] = $this->input->post('user');
        $user = $this->input->post('user');
        $q = $this->db->get_where("tbl_user_login", $id);
        $set_detail = $q->row();
        foreach ($q->result() as $data) {
            $d['id_user_login'] = $data->id_user_login;
            $d['username'] = $data->username;
            $d['nama_lengkap'] = $data->nama_lengkap;
            $d['nip'] = $data->nip;
            $d['jabatan'] = $data->jabatan;
        }
        ob_start();
        $content = $this->load->view('cetak/mengemudi_print', $d);
        $content = ob_get_clean();
        $this->load->library('html2pdf');
        try {
            $html2pdf = new HTML2PDF('L', 'F4', 'en');
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output('Hasil Tes Mengemudi.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    public function wawancara() {
        $user = $this->input->post('user');
        $id['username'] = $this->input->post('user');
        $q = $this->db->get_where("tbl_user_login", $id);
        $set_detail = $q->row();
        foreach ($q->result() as $data) {
            $d['id_user_login'] = $data->id_user_login;
            $d['username'] = $data->username;
            $d['nama_lengkap'] = $data->nama_lengkap;
            $d['nip'] = $data->nip;
            $d['jabatan'] = $data->jabatan;
        }
        $d['data_peserta'] = $this->app_login_model->getCetakWawancara($user);
        ob_start();
        $content = $this->load->view('cetak/wawancara_print', $d);
        $content = ob_get_clean();
        $this->load->library('html2pdf');
        try {
            $html2pdf = new HTML2PDF('L', 'A3', 'en');
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output('Hasil Tes Wawancara.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

}

/* End of file cetak.php */
/* Location: ./application/controllers/cetak.php */