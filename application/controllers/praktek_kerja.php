<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Praktek_Kerja extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_ujian.*, tbl_berkas.* FROM tbl_data_peserta, tbl_ujian, tbl_berkas WHERE tbl_data_peserta.no_peserta = tbl_ujian.no_peserta AND tbl_ujian.no_peserta = tbl_berkas.no_peserta");
            $config['base_url'] = base_url() . 'praktek_kerja/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_ujian.*, tbl_berkas.* FROM tbl_data_peserta, tbl_ujian, tbl_berkas WHERE tbl_data_peserta.no_peserta = tbl_ujian.no_peserta AND tbl_ujian.no_peserta = tbl_berkas.no_peserta LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'praktek_kerja/praktek_kerja_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT tbl_data_peserta.*, tbl_ujian.*, tbl_berkas.* FROM tbl_data_peserta, tbl_ujian, tbl_berkas WHERE tbl_data_peserta.no_peserta = tbl_ujian.no_peserta AND tbl_ujian.no_peserta = tbl_berkas.no_peserta AND nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp");
            $config['base_url'] = base_url() . 'praktek_kerja/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT tbl_data_peserta.*, tbl_ujian.*, tbl_berkas.* FROM tbl_data_peserta, tbl_ujian, tbl_berkas WHERE tbl_data_peserta.no_peserta = tbl_ujian.no_peserta AND tbl_ujian.no_peserta = tbl_berkas.no_peserta AND nama_ktp LIKE '%" . $kata . "%' ORDER BY nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'praktek_kerja/praktek_kerja_home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function hadir() {
        if (($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") || ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_pusat")) {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $id['id_peserta'] = decrypt_url($this->uri->segment(3));
            $this->session->set_userdata($id);
            $data_peserta = $this->db->get_where("tbl_data_peserta", $id);

            if ($data_peserta->num_rows() > 0) {
                $q = $this->db->get_where("tbl_data_peserta", $id);
                $set_detail = $q->row();
                $this->session->set_userdata("nama_ktp", $set_detail->nama_ktp);
                foreach ($q->result() as $data) {
                    $d['id_peserta'] = $data->id_peserta;
                    $d['nama_ktp'] = $data->nama_ktp;
                    $d['nik'] = $data->nik;
                    $d['no_peserta'] = $data->no_peserta;
                    $d['jenis_kelamin'] = $data->jenis_kelamin;
                    $d['jabatan_nama'] = $data->jabatan_nama;
                    $d['jenis_formasi'] = $data->jenis_formasi;
                    $d['satker'] = $data->satker;
                    $d['lembaga_pendidikan'] = $data->lembaga_pendidikan;
                }

                $d['main'] = 'praktek_kerja/praktek_kerja_hadir';
                $this->load->vars($d);
                $this->load->view('template/template');
            } else {
                header('location:' . base_url() . '');
            }
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function tidak_hadir() {
        if (($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") || ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_pusat")) {

            $in['id_peserta'] = decrypt_url($this->uri->segment(3));
            $in['keterangan'] = "TIDAK HADIR TES KETERAMPILAN KOMPUTER";
            $in['penguji'] = $this->session->userdata('username');
            $in['created_time'] = date('Y-m-d H:i:s');

            $this->db->insert("tbl_nilai_praktek_kerja", $in);
            header('location:' . base_url() . 'praktek_kerja/index/');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function simpan_nilai() {
        if (($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") || ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_pusat")) {
            $id_peserta = $this->input->post("id_peserta");
            $query = $this->db->get_where('tbl_nilai_praktek_kerja', array('id_peserta' => $id_peserta));
            if ($query->num_rows() == 0) {
                $in['id_peserta'] = addslashes($this->input->post("id_peserta"));
                $in['kecepatan_durasi'] = addslashes($this->input->post('kecepatan_durasi'));
                $in['ketelitian'] = addslashes($this->input->post('ketelitian'));
                $in['kerapian'] = addslashes($this->input->post('kerapian'));
                $in['penguasaan_program'] = addslashes($this->input->post('penguasaan_program'));
                $in['penguji'] = $this->session->userdata('username');
                $in['created_time'] = date('Y-m-d H:i:s');

                $this->db->insert("tbl_nilai_praktek_kerja", $in);
                redirect('praktek_kerja/index', 'refresh');
            } else {
                redirect('praktek_kerja/index', 'refresh');
            }
        } else {
            header('location:' . base_url() . '');
        }
    }

}

/* End of file praktek_kerja.php */
/* Location: ./application/controllers/praktek_kerja.php */