<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tanggal_Ujian extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGAWAL TAHANAN%'");
            $config['base_url'] = base_url() . 'tanggal_ujian/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGAWAL TAHANAN%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGAWAL TAHANAN%' AND kode_satker = " . $kode_satker . "");
            $config['base_url'] = base_url() . 'tanggal_ujian/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGAWAL TAHANAN%' AND kode_satker = " . $kode_satker . " ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGAWAL TAHANAN%' AND nama_ktp LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGAWAL TAHANAN%' AND nama_ktp LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE kode_satker = '$kode_satker' AND jabatan_nama LIKE '%PENGAWAL TAHANAN%' AND nama_ktp LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/index/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE kode_satker = '$kode_satker' AND jabatan_nama LIKE '%PENGAWAL TAHANAN%' AND nama_ktp LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/home';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function beladiri() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_beladiri IS NULL");
            $config['base_url'] = base_url() . 'tanggal_ujian/beladiri/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_beladiri IS NULL ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/beladiri';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_beladiri IS NULL AND kode_satker = " . $kode_satker . "");
            $config['base_url'] = base_url() . 'tanggal_ujian/beladiri/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_beladiri IS NULL AND kode_satker = " . $kode_satker . " ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/beladiri';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function beladiri_cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/beladiri/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/beladiri';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND kode_satker = '$kode_satker' AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/beladiri/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND kode_satker = '$kode_satker' AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/beladiri';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function samapta() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGADMINISTRASI PENANGANAN PERKARA' OR jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_samapta IS NULL");
            $config['base_url'] = base_url() . 'tanggal_ujian/samapta/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGADMINISTRASI PENANGANAN PERKARA' OR jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_samapta IS NULL ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/samapta';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGADMINISTRASI PENANGANAN PERKARA' OR jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_samapta IS NULL AND kode_satker = " . $kode_satker . "");
            $config['base_url'] = base_url() . 'tanggal_ujian/samapta/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGADMINISTRASI PENANGANAN PERKARA' OR jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_samapta IS NULL AND kode_satker = " . $kode_satker . " ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/samapta';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function samapta_cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGADMINISTRASI PENANGANAN PERKARA' OR jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_samapta IS NULL AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/samapta/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGADMINISTRASI PENANGANAN PERKARA' OR jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_samapta IS NULL AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/samapta';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGADMINISTRASI PENANGANAN PERKARA' OR jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_samapta IS NULL AND kode_satker = '$kode_satker' AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/samapta/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'PENGADMINISTRASI PENANGANAN PERKARA' OR jabatan_nama = 'PENGAWAL TAHANAN ATAU NARAPIDANA' AND tgl_samapta IS NULL AND kode_satker = '$kode_satker' AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/samapta';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function praktek_kerja() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGADMINISTRASI PENANGANAN PERKARA%' AND tgl_praktek_kerja IS NULL");
            $config['base_url'] = base_url() . 'tanggal_ujian/praktek_kerja/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGADMINISTRASI PENANGANAN PERKARA%' AND tgl_praktek_kerja IS NULL ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/praktek_kerja';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGADMINISTRASI PENANGANAN PERKARA%' AND tgl_praktek_kerja IS NULL AND kode_satker = " . $kode_satker . "");
            $config['base_url'] = base_url() . 'tanggal_ujian/praktek_kerja/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGADMINISTRASI PENANGANAN PERKARA%' AND tgl_praktek_kerja IS NULL AND kode_satker = " . $kode_satker . " ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/praktek_kerja';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function praktek_kerja_cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGADMINISTRASI PENANGANAN PERKARA%' AND tgl_praktek_kerja IS NULL AND nama_ktp LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/praktek_kerja/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGADMINISTRASI PENANGANAN PERKARA%' AND tgl_praktek_kerja IS NULL AND nama_ktp LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/praktek_kerja';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $kode_satker = $this->session->userdata("kode_satker");
            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGADMINISTRASI PENANGANAN PERKARA%' AND tgl_praktek_kerja IS NULL AND kode_satker = '$kode_satker' AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/praktek_kerja/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama LIKE '%PENGADMINISTRASI PENANGANAN PERKARA%' AND tgl_praktek_kerja IS NULL AND kode_satker = '$kode_satker' AND nama_ktp LIKE '%" . $kata . "%' OR no_peserta LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/praktek_kerja';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function wawancara() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'ANALIS RANCANGAN NASKAH PERJANJIAN' OR jabatan_nama LIKE '%AHLI PERTAMA%' AND tgl_wawancara IS NULL AND id_user IS NULL");
            $config['base_url'] = base_url() . 'tanggal_ujian/wawancara/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['user'] = $this->app_login_model->getAllUserPewawancara();
            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE jabatan_nama = 'ANALIS RANCANGAN NASKAH PERJANJIAN' OR jabatan_nama LIKE '%AHLI PERTAMA%' AND tgl_wawancara IS NULL AND id_user IS NULL ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . "," . $limit . "");
            $d['main'] = 'tanggal_ujian/wawancara';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function wawancara_cari() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $tgl_sekarang = date("Y-m-d");

            if ($this->input->post("cari") == "") {
                $kata = $this->session->userdata('kata');
            } else {
                $sess_data['kata'] = $this->input->post("cari");
                $this->session->set_userdata($sess_data);
                $kata = $this->session->userdata('kata');
            }

            $page = $this->uri->segment(3);
            $limit = $this->config->item('limit_data');
            if (!$page):
                $offset = 0;
            else:
                $offset = $page;
            endif;

            $d['tot'] = $offset;
            $tot_hal = $this->db->query("SELECT * FROM tbl_data_peserta WHERE tgl_wawancara IS NULL AND id_user IS NULL AND nama_ktp LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp");
            $config['base_url'] = base_url() . 'tanggal_ujian/wawancara/';
            $config['total_rows'] = $tot_hal->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $this->pagination->initialize($config);
            $d["paginator"] = $this->pagination->create_links();

            $start = (int) $this->uri->segment(3) + 1;
            $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) + $config['per_page'];

            $d['result_count'] = "Jumlah Data " . $config['total_rows'] . "";

            $d['user'] = $this->app_login_model->getAllUserPewawancara();
            $d['data_peserta'] = $this->db->query("SELECT * FROM tbl_data_peserta WHERE tgl_wawancara IS NULL AND id_user IS NULL AND nama_ktp LIKE '%" . $kata . "%' ORDER BY jabatan_nama, nama_ktp LIMIT " . $offset . ", " . $limit . "");
            $d['main'] = 'tanggal_ujian/wawancara';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    function beladiri_ubah() {
        $id_peserta = $this->input->post('id_peserta');
        $tgl_beladiri = $this->input->post('tgl_beladiri');
        $this->app_login_model->ubahBeladiri($id_peserta, $tgl_beladiri);
        redirect('tanggal_ujian/beladiri');
    }

    function samapta_ubah() {
        $id_peserta = $this->input->post('id_peserta');
        $tgl_samapta = $this->input->post('tgl_samapta');
        $this->app_login_model->ubahMengemudi($id_peserta, $tgl_samapta);
        redirect('tanggal_ujian/samapta');
    }

    function praktek_kerja_ubah() {
        $id_peserta = $this->input->post('id_peserta');
        $tgl_praktek_kerja = $this->input->post('tgl_praktek_kerja');
        $this->app_login_model->ubahKomputer($id_peserta, $tgl_praktek_kerja);
        redirect('tanggal_ujian/praktek_kerja');
    }

    function wawancara_ubah() {
        $id_peserta = $this->input->post('id_peserta');
        $id_user = $this->input->post('id_user');
        $tgl_wawancara = $this->input->post('tgl_wawancara');
        $this->app_login_model->ubahWawancara($id_peserta, $tgl_wawancara, $id_user);
        redirect('tanggal_ujian/wawancara');
    }

    public function cetak() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") {
            $kode_satker = $this->session->userdata("kode_satker");
            $d['data_peserta'] = $this->app_login_model->getCetakAll($kode_satker);
            ob_start();
            $content = $this->load->view('tanggal_ujian/print', $d);
            $content = ob_get_clean();
            $this->load->library('html2pdf');
            try {
                $html2pdf = new HTML2PDF('L', 'F4', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
                $html2pdf->Output('Cetak Tanggal Ujian.pdf');
            } catch (HTML2PDF_exception $e) {
                echo $e;
                exit;
            }
        } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['data_peserta'] = $this->app_login_model->getCetakAllAdmin();
            ob_start();
            $content = $this->load->view('tanggal_ujian/print', $d);
            $content = ob_get_clean();
            $this->load->library('html2pdf');
            try {
                $html2pdf = new HTML2PDF('L', 'F4', 'en');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
                $html2pdf->Output('Cetak Tanggal Ujian.pdf');
            } catch (HTML2PDF_exception $e) {
                echo $e;
                exit;
            }
        }
    }

    public function pewawancara() {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") {
            $d['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $d['judul_pendek'] = $this->config->item('nama_aplikasi_pendek');
            $d['instansi'] = $this->config->item('nama_instansi');
            $d['credit'] = $this->config->item('credit_aplikasi');
            $d['alamat'] = $this->config->item('alamat_instansi');

            $d['user_wawancara'] = $this->app_login_model->getAllUserPewawancara();

            $d['main'] = 'tanggal_ujian/pewawancara';
            $this->load->vars($d);
            $this->load->view('template/template');
        } else {
            header('location:' . base_url() . '');
        }
    }

    public function cetak_pewawancara() {
        $user = $this->input->post('user');
        $d['data_peserta'] = $this->app_login_model->getCetakPewawancara($user);
        $id['id_user_login'] = $this->input->post('user');
        $q = $this->db->get_where("tbl_user_login", $id);
        $set_detail = $q->row();
        foreach ($q->result() as $data) {
            $d['id_user_login'] = $data->id_user_login;
            $d['username'] = $data->username;
            $d['nama_lengkap'] = $data->nama_lengkap;
            $d['nip'] = $data->nip;
            $d['jabatan'] = $data->jabatan;
        }
        ob_start();
        $content = $this->load->view('tanggal_ujian/pewawancara_print', $d);
        $content = ob_get_clean();
        $this->load->library('html2pdf');
        try {
            $html2pdf = new HTML2PDF('P', 'F4', 'en');
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output('Daftar Nama Peserta Wawancara.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

}

/* End of file tanggal_ujian.php */
/* Location: ./application/controllers/tanggal_ujian.php */