<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('database', 'session', 'form_validation', 'pagination');
$autoload['helper'] = array('url', 'form', 'security', 'text');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array('app_login_model');


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */
