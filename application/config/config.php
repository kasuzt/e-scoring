<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

function datetimes($tgl, $Jam = true) {
    /* Contoh Format : 2007-08-15 01:27:45 */
    $tanggal = strtotime($tgl);
    $bln_array = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    );
    $hari_arr = Array('0' => 'Minggu',
        '1' => 'Senin',
        '2' => 'Selasa',
        '3' => 'Rabu',
        '4' => 'Kamis',
        '5' => 'Jum`at',
        '6' => 'Sabtu'
    );
    $hari = $hari_arr[date('w', $tanggal)];
    $tggl = str_pad(date('j', $tanggal), 2, '00', STR_PAD_LEFT);
    $bln = $bln_array[date('m', $tanggal)];
    $thn = date('Y', $tanggal);
    $jam = $Jam ? date('H:i:s', $tanggal) : '';
    return "$tggl $bln $thn <br>$jam";
}

function datedoank($tgl, $Jam = true) {
    /* Contoh Format : 2007-08-15 01:27:45 */
    $tanggal = strtotime($tgl);
    $bln_array = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    );
    $hari_arr = Array('0' => 'Minggu',
        '1' => 'Senin',
        '2' => 'Selasa',
        '3' => 'Rabu',
        '4' => 'Kamis',
        '5' => 'Jum`at',
        '6' => 'Sabtu'
    );
    $hari = $hari_arr[date('w', $tanggal)];
    $tggl = str_pad(date('j', $tanggal), 2, '00', STR_PAD_LEFT);
    $bln = $bln_array[date('m', $tanggal)];
    $thn = date('Y', $tanggal);
    $jam = $Jam ? date('H:i:s', $tanggal) : '';
    return "$tggl $bln $thn";
}

$http = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '') . '://';
$fo = str_replace("index.php", "", $_SERVER['SCRIPT_NAME']);
$config['base_url'] = "$http" . $_SERVER['SERVER_NAME'] . "" . $fo;
$config['index_page'] = '';
$config['uri_protocol'] = 'AUTO';
$config['url_suffix'] = '';
$config['language'] = 'english';
$config['charset'] = 'UTF-8';
$config['enable_hooks'] = FALSE;
$config['subclass_prefix'] = 'MY_';
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-@\=';
$config['allow_get_array'] = TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger'] = 'c';
$config['function_trigger'] = 'm';
$config['directory_trigger'] = 'd';
$config['log_threshold'] = 0;
$config['log_path'] = '';
$config['log_date_format'] = 'Y-m-d H:i:s';
$config['cache_path'] = '';
$config['encryption_key'] = 'app_simpeg';
$config['sess_cookie_name'] = 'ci_session';
$config['sess_expiration'] = 7200;
$config['sess_expire_on_close'] = FALSE;
$config['sess_encrypt_cookie'] = FALSE;
$config['sess_use_database'] = FALSE;
$config['sess_table_name'] = 'ci_sessions';
$config['sess_match_ip'] = FALSE;
$config['sess_match_useragent'] = TRUE;
$config['sess_time_to_update'] = 300;
$config['cookie_prefix'] = "";
$config['cookie_domain'] = "";
$config['cookie_path'] = "/";
$config['cookie_secure'] = FALSE;
$config['global_xss_filtering'] = TRUE;
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;
$config['compress_output'] = FALSE;
$config['time_reference'] = 'local';
$config['rewrite_short_tags'] = FALSE;
$config['proxy_ips'] = '';

$config['limit_data'] = '10';
$config['nama_aplikasi_full'] = 'e-Scoring';
$config['nama_aplikasi_pendek'] = 'e-Scoring ';
$config['nama_instansi'] = 'Biro Kepegawaian';
$config['alamat_instansi'] = 'Kejaksaan Republik Indonesia';
$config['credit_aplikasi'] = 'e-Scoring - Biro Kepegawaian Kejaksaan Republik Indonesia 2022';


/* End of file config.php */
/* Location: ./application/config/config.php */
