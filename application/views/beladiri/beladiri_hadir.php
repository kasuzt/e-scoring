<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#"><?php echo $nama_ktp; ?> [<?php echo $jabatan_nama; ?> - <?php echo $jenis_formasi; ?>]</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <form class="form-horizontal">
        <div class="control-group">
            <div class="span3"><strong>FOTO</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <img src="<?php echo base_url(); ?>uploads/<?php echo $no_peserta; ?> <?php echo $nama_ktp; ?>.jpg" width="200" />
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>NO. PESERTA</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="no_peserta" id="no_peserta" value="<?php echo $no_peserta; ?>" placeholder="NO. PESERTA">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>JENIS KELAMIN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="jenis_kelamin" id="jenis_kelamin" value="<?php echo $jenis_kelamin; ?>" placeholder="JENIS KELAMIN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LEMBAGA PENDIDIKAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="lembaga_pendidikan" id="lembaga_pendidikan" value="<?php echo $lembaga_pendidikan; ?>" placeholder="LEMBAGA PENDIDIKAN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LOKASI UJIAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="satker" id="satker" value="<?php echo $satker; ?>" placeholder="LOKASI UJIAN">
            </div>
        </div>
    </form>

    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Formulir Tes Keterampilan Beladiri</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <?php if (validation_errors()) { ?>
        <div class="alert alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Terjadi Kesalahan!</h4>
            <?php echo validation_errors(); ?>
        </div>
    <?php } ?>

    <form action="<?php echo base_url(); ?>beladiri/simpan_nilai" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">

        <div class="control-group">
            <div class="span3"><strong>SIKAP PASANG / KUDA-KUDA / YOI</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <select id="kuda_kuda" name="kuda_kuda" class="validate[required] text-input">
                    <option value="">--- Pilih Nilai ---</option>
                    <option value="BAIK SEKALI">BAIK SEKALI</option>
                    <option value="BAIK">BAIK</option>
                    <option value="CUKUP">CUKUP</option>
                    <option value="KURANG">KURANG</option>
                    <option value="KURANG SEKALI">KURANG SEKALI</option>
                </select>
                <label id="lbl_1"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>TANGKISAN / COUNTER / BLOK</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <select id="tangkisan" name="tangkisan" class="validate[required] text-input">
                    <option value="">--- Pilih Nilai ---</option>
                    <option value="BAIK SEKALI">BAIK SEKALI</option>
                    <option value="BAIK">BAIK</option>
                    <option value="CUKUP">CUKUP</option>
                    <option value="KURANG">KURANG</option>
                    <option value="KURANG SEKALI">KURANG SEKALI</option>
                </select>
                <label id="lbl_2"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>PUKULAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <select id="pukulan" name="pukulan" class="validate[required] text-input">
                    <option value="">--- Pilih Nilai ---</option>
                    <option value="BAIK SEKALI">BAIK SEKALI</option>
                    <option value="BAIK">BAIK</option>
                    <option value="CUKUP">CUKUP</option>
                    <option value="KURANG">KURANG</option>
                    <option value="KURANG SEKALI">KURANG SEKALI</option>
                </select>
                <label id="lbl_3"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>TENDANGAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <select id="tendangan" name="tendangan" class="validate[required] text-input">
                    <option value="">--- Pilih Nilai ---</option>
                    <option value="BAIK SEKALI">BAIK SEKALI</option>
                    <option value="BAIK">BAIK</option>
                    <option value="CUKUP">CUKUP</option>
                    <option value="KURANG">KURANG</option>
                    <option value="KURANG SEKALI">KURANG SEKALI</option>
                </select>
                <label id="lbl_4"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>RANGKAIAN GERAK / JURUS / KATA</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <select id="rangkaian_gerak" name="rangkaian_gerak" class="validate[required] text-input">
                    <option value="">--- Pilih Nilai ---</option>
                    <option value="BAIK SEKALI">BAIK SEKALI</option>
                    <option value="BAIK">BAIK</option>
                    <option value="CUKUP">CUKUP</option>
                    <option value="KURANG">KURANG</option>
                    <option value="KURANG SEKALI">KURANG SEKALI</option>
                </select>
                <label id="lbl_5"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>CATATAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <textarea name="keterangan" class="span6 validate[required] text-input"></textarea><span class="help-inline" style="color:red;"><em><strong>Dapat di isi misalnya, kondisi disabilitas / hamil, prestasi yang pernah diraih, dll.</strong></em></span>
                <label id="lbl_6"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="controls" align="center">
                <button type="submit" class="btn btn-primary" onClick="return confirm('Anda yakin data nilai tes keterampilan beladiri sudah benar ?');">Simpan Data</button>
                <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
            </div>
        </div>

        <input type="hidden" name="id_peserta" value="<?php echo $id_peserta; ?>">
        <script type="text/javascript">
            $(".chzn-select").chosen();
        </script>

    </form>

</div>

<script type="text/javascript">
    jQuery.fn.ForceNumericOnly =
            function ()
            {
                return this.each(function ()
                {
                    $(this).keydown(function (event)
                    {
                        x = $(this).val($(this).val().replace(/[^\d].+/, ""));
                        if ((event.which < 48 || event.which > 57)) {
                            event.preventDefault();
                        }
                        return x;
                    });
                });
            };

    $('.formatMataUang').ForceNumericOnly();

    function declarenilai(n) {
        var res = "";
        if (n >= 51 && n <= 60) {
            res = "Kurang Sekali";
        } else if (n >= 61 && n <= 70) {
            res = "Kurang";
        } else if (n >= 71 && n <= 80) {
            res = "Cukup";
        } else if (n >= 81 && n <= 90) {
            res = "Baik";
        } else if (n >= 91 && n <= 100) {
            res = "Baik Sekali";
        } else {
            res = "Dibawah 50";
        }
        return res;
    }

    $("#kuda_kuda_").blur(function () {
        var nilai = $(this).val();
        if (nilai < 51 || nilai > 100) {
            alert('Nilai harus diantara 51 s/d 100');
            $("#kuda_kuda_").val('');
        } else {
            $("#lbl_1").text(declarenilai(nilai));
        }
    });

    $("#tangkisan_").blur(function () {
        var nilai = $(this).val();
        if (nilai < 51 || nilai > 100) {
            alert('Nilai harus diantara 51 s/d 100');
            $("#tangkisan_").val('');
        } else {
            $("#lbl_2").text(declarenilai(nilai));
        }
    });

    $("#pukulan_").blur(function () {
        var nilai = $(this).val();
        if (nilai < 51 || nilai > 100) {
            alert('Nilai harus diantara 51 s/d 100');
            $("#pukulan_").val('');
        } else {
            $("#lbl_3").text(declarenilai(nilai));
        }
    });

    $("#tendangan_").blur(function () {
        var nilai = $(this).val();
        if (nilai < 51 || nilai > 100) {
            alert('Nilai harus diantara 51 s/d 100');
            $("#tendangan_").val('');
            $('.formatMataUang').number(true, 2);
        } else {
            $("#lbl_4").text(declarenilai(nilai));
        }
    });

    $("#rangkaian_gerak_").blur(function () {
        var nilai = $(this).val();
        if (nilai < 51 || nilai > 100) {
            alert('Nilai harus diantara 51 s/d 100');
            $("#rangkaian_gerak_").val('');
            $('.formatMataUang').number(true, 2);
        } else {
            $("#lbl_5").text(declarenilai(nilai));
        }
    });

</script>
