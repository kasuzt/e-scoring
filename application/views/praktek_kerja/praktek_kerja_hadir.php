<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#"><?php echo $nama_ktp; ?> [<?php echo $jabatan_nama; ?> - <?php echo $jenis_formasi; ?>]</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <form class="form-horizontal">
        <div class="control-group">
            <div class="span3"><strong>FOTO</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <img src="<?php echo base_url(); ?>uploads/<?php echo $no_peserta; ?> <?php echo $nama_ktp; ?>.jpg" width="200" />
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>NO. PESERTA</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="no_peserta" id="no_peserta" value="<?php echo $no_peserta; ?>" placeholder="NO. PESERTA">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>JENIS KELAMIN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="jenis_kelamin" id="jenis_kelamin" value="<?php echo $jenis_kelamin; ?>" placeholder="JENIS KELAMIN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LEMBAGA PENDIDIKAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="lembaga_pendidikan" id="lembaga_pendidikan" value="<?php echo $lembaga_pendidikan; ?>" placeholder="LEMBAGA PENDIDIKAN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LOKASI UJIAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="satker" id="satker" value="<?php echo $satker; ?>" placeholder="LOKASI UJIAN">
            </div>
        </div>
    </form>

    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Formulir Tes Keterampilan Komputer</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <?php if (validation_errors()) { ?>
        <div class="alert alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Terjadi Kesalahan!</h4>
            <?php echo validation_errors(); ?>
        </div>
    <?php } ?>

    <form action="<?php echo base_url(); ?>komputer/simpan_nilai" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">

        <div class="control-group">
            <div class="span3"><strong>KECEPATAN / DURASI WAKTU</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" class="span1 validate[required,custom[number]] text-input formatMataUang" name="kecepatan_durasi" id="kecepatan_durasi">
                <span class="help-inline" style="color:red;"><em><strong>81 – 90 = Baik, 71 – 80 = Sedang, 61 – 70 = Cukup</strong></em></span>
                <label id="lbl_1"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>KETELITIAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" class="span1 validate[required,custom[number]] text-input formatMataUang" name="ketelitian" id="ketelitian">
                <span class="help-inline" style="color:red;"><em><strong>81 – 90 = Baik, 71 – 80 = Sedang, 61 – 70 = Cukup</strong></em></span>
                <label id="lbl_2"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>KERAPIAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" class="span1 validate[required,custom[number]] text-input formatMataUang" name="kerapian" id="kerapian">
                <span class="help-inline" style="color:red;"><em><strong>81 – 90 = Baik, 71 – 80 = Sedang, 61 – 70 = Cukup</strong></em></span>
                <label id="lbl_3"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>PENGUASAAN PROGRAM Ms. OFFICE</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" class="span1 validate[required,custom[number]] text-input formatMataUang" name="penguasaan_program" id="penguasaan_program">
                <span class="help-inline" style="color:red;"><em><strong>81 – 90 = Baik, 71 – 80 = Sedang, 61 – 70 = Cukup</strong></em></span>
                <label id="lbl_4"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="controls" align="center">
                <button type="submit" class="btn btn-primary" onClick="return confirm('Anda yakin data nilai tes keterampilan komputer sudah benar ?');">Simpan Data</button>
                <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
            </div>
        </div>

        <input type="hidden" name="id_peserta" value="<?php echo $id_peserta; ?>">
        <script type="text/javascript">
            $(".chzn-select").chosen();
        </script>

    </form>

</div>
<script type="text/javascript">
    jQuery.fn.ForceNumericOnly =
            function ()
            {
                return this.each(function ()
                {
                    $(this).keydown(function (event)
                    {
                        x = $(this).val($(this).val().replace(/[^\d].+/, ""));
                        if ((event.which < 48 || event.which > 57)) {
                            event.preventDefault();
                        }
                        return x;
                    });
                });
            };

    $('.formatMataUang').ForceNumericOnly();

    function declarenilai(n) {
        var res = "";
        if (n >= 61 && n <= 70) {
            res = "Cukup";
        } else if (n >= 71 && n <= 80) {
            res = "Sedang";
        } else if (n >= 81 && n <= 90) {
            res = "Baik";
        } else if (n >= 91) {
            res = "Sangat Baik";
        } else {
            res = "Dibawah 60";
        }
        return res;
    }

    $("#kecepatan_durasi").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#kecepatan_durasi").val('');
        } else {
            $("#lbl_1").text(declarenilai(nilai));
        }
    });

    $("#ketelitian").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#ketelitian").val('');
        } else {
            $("#lbl_2").text(declarenilai(nilai));
        }
    });

    $("#kerapian").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#kerapian").val('');
        } else {
            $("#lbl_3").text(declarenilai(nilai));
        }
    });

    $("#penguasaan_program").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#penguasaan_program").val('');
        } else {
            $("#lbl_4").text(declarenilai(nilai));
        }
    });

</script>
