<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?php echo base_url(); ?>nilai/komputer">Pelamar Ujian SKB - Tes Komputer</a>
                <div class="span6 pull-right">
                    <?php echo form_open("nilai/komputer_cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span2" name="cari" placeholder="">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Pencarian</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <div align="center">
            <ul>
                <strong><?php echo $result_count; ?></strong>
            </ul>
        </div>
        <table class="table table-hover table-striped table-responsive">
            <thead>
                <tr>
                    <th style="vertical-align: top;"><div align="center">No.</div></th>
                    <th style="vertical-align: top;"><div align="center">Peserta</div></th>
                    <th style="vertical-align: top;"><div align="center">Kecepatan Durasi</div></th>
                    <th style="vertical-align: top;"><div align="center">Ketelitian</div></th>
                    <th style="vertical-align: top;"><div align="center">Kerapian</div></th>
                    <th style="vertical-align: top;"><div align="center">Penguasaan Program</div></th>
                    <th style="vertical-align: top;"><div align="center">Keterangan</div></th>
                    <th style="vertical-align: top;"><div align="center">Dibuat</div></th>
                    <th style="vertical-align: top;"><div align="center">Penguji</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data_peserta->result_array())) {
                    $no = $tot + 1;
                    foreach ($data_peserta->result_array() as $dp) {
                        ?>
                        <tr>
                            <td><div align="center"><?php echo $no; ?>.</div></td>
                            <td><div align="center"><img src="<?php echo base_url(); ?>uploads/<?php echo $dp['no_peserta']; ?> <?php echo $dp['nama_ktp']; ?>.jpg" width="150" /><br /><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                                    <?php echo $dp['no_peserta']; ?><br />
                                    <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br /><?php echo $dp['satker']; ?></div>
                            </td>
                            <td><div align="center"><?php echo $dp['kecepatan_durasi']; ?></div></td>
                            <td><div align="center"><?php echo $dp['ketelitian']; ?></div></td>
                            <td><div align="center"><?php echo $dp['kerapian']; ?></div></td>
                            <td><div align="center"><?php echo $dp['penguasaan_program']; ?></div></td>
                            <td><div align="center"><?php echo $dp['keterangan']; ?></div></td>
                            <td><div align="center"><?php echo datetimes($dp['created_time']); ?></div></td>
                            <td><div align="center"><?php echo $dp['penguji']; ?></div></td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "<tr><td colspan='9'><h4><div align='center'>Data Tidak ada !!!</div></h4></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
                <?php
                echo $paginator;
                ?>
            </ul>
        </div>
    </section>
</div>
