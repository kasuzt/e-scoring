<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?php echo base_url(); ?>nilai/wawancara">Pelamar Ujian SKB - Tes Wawancara</a>
                <div class="span6 pull-right">
                    <?php echo form_open("nilai/wawancara_cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span2" name="cari" placeholder="">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Pencarian</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <div align="center">
            <ul>
                <strong><?php echo $result_count; ?></strong>
            </ul>
        </div>
        <table class="table table-hover table-striped table-responsive">
            <thead>
                <tr>
                    <th style="vertical-align: top;"><div align="center">No.</div></th>
                    <th style="vertical-align: top;"><div align="center">Peserta</div></th>
                    <th style="vertical-align: top;"><div align="center">Pengetahuan Umum</div></th>
                    <th style="vertical-align: top;"><div align="center">Pengetahuan Akademik</div></th>
                    <th style="vertical-align: top;"><div align="center">Motivasi Bekerja</div></th>
                    <th style="vertical-align: top;"><div align="center">Pengambilan Keputusan</div></th>
                    <th style="vertical-align: top;"><div align="center">Penampilan Sikap</div></th>
                    <th style="vertical-align: top;"><div align="center">Kemampuan Verbal</div></th>
                    <th style="vertical-align: top;"><div align="center">Keterangan</div></th>
                    <th style="vertical-align: top;"><div align="center">Dibuat</div></th>
                    <th style="vertical-align: top;"><div align="center">Penguji</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data_peserta->result_array())) {
                    $no = $tot + 1;
                    foreach ($data_peserta->result_array() as $dp) {
                        ?>
                        <tr>
                            <td><div align="center"><?php echo $no; ?>.</div></td>
                            <td><div align="center"><img src="<?php echo base_url(); ?>uploads/<?php echo $dp['no_peserta']; ?> <?php echo $dp['nama_ktp']; ?>.jpg" width="150" /><br /><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                                    <?php echo $dp['no_peserta']; ?><br />
                                    <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br /><?php echo $dp['satker']; ?></div>
                            </td>
                            <td><div align="center"><?php echo $dp['pengetahuan_umum']; ?></div></td>
                            <td><div align="center"><?php echo $dp['pengetahuan_akademik']; ?></div></td>
                            <td><div align="center"><?php echo $dp['motivasi_bekerja']; ?></div></td>
                            <td><div align="center"><?php echo $dp['pengambilan_keputusan']; ?></div></td>
                            <td><div align="center"><?php echo $dp['penampilan_sikap']; ?></div></td>
                            <td><div align="center"><?php echo $dp['kemampuan_verbal']; ?></div></td>
                            <td><div align="center"><?php echo $dp['keterangan']; ?></div></td>
                            <td><div align="center"><?php echo datetimes($dp['created_time']); ?></div></td>
                            <td><div align="center"><?php echo $dp['penguji']; ?></div></td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "<tr><td colspan='11'><h4><div align='center'>Data Tidak ada !!!</div></h4></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
                <?php
                echo $paginator;
                ?>
            </ul>
        </div>
    </section>
</div>
