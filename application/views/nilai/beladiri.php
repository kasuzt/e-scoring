<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?php echo base_url(); ?>nilai/beladiri">Pelamar Ujian SKB - Tes Beladiri</a>
                <div class="span6 pull-right">
                    <?php echo form_open("nilai/beladiri_cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span2" name="cari" placeholder="">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Pencarian</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <div align="center">
            <ul>
                <strong><?php echo $result_count; ?></strong>
            </ul>
        </div>
        <table class="table table-hover table-striped table-responsive">
            <thead>
                <tr>
                    <th style="vertical-align: top;"><div align="center">No.</div></th>
                    <th style="vertical-align: top;"><div align="center">Peserta</div></th>
                    <th style="vertical-align: top;"><div align="center">Kuda-Kuda</div></th>
                    <th style="vertical-align: top;"><div align="center">Tangkisan</div></th>
                    <th style="vertical-align: top;"><div align="center">Pukulan</div></th>
                    <th style="vertical-align: top;"><div align="center">Tendangan</div></th>
                    <th style="vertical-align: top;"><div align="center">Rangkaian Gerak</div></th>
                    <th style="vertical-align: top;"><div align="center">Keterangan</div></th>
                    <th style="vertical-align: top;"><div align="center">Dibuat</div></th>
                    <th style="vertical-align: top;"><div align="center">Penguji</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data_peserta->result_array())) {
                    $no = $tot + 1;
                    foreach ($data_peserta->result_array() as $dp) {
                        ?>
                        <tr>
                            <td><div align="center"><?php echo $no; ?>.</div></td>
                            <td><div align="center"><img src="<?php echo base_url(); ?>uploads/<?php echo $dp['no_peserta']; ?> <?php echo $dp['nama_ktp']; ?>.jpg" width="150" /><br /><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                                    <?php echo $dp['no_peserta']; ?><br />
                                    <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br /><?php echo $dp['satker']; ?></div>
                            </td>
                            <td><div align="center"><?php echo $dp['kuda_kuda']; ?></div></td>
                            <td><div align="center"><?php echo $dp['tangkisan']; ?></div></td>
                            <td><div align="center"><?php echo $dp['pukulan']; ?></div></td>
                            <td><div align="center"><?php echo $dp['tendangan']; ?></div></td>
                            <td><div align="center"><?php echo $dp['rangkaian_gerak']; ?></div></td>
                            <td><div align="center"><?php echo $dp['keterangan']; ?></div></td>
                            <td><div align="center"><?php echo datetimes($dp['created_time']); ?></div></td>
                            <td><div align="center"><?php echo $dp['penguji']; ?></div></td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "<tr><td colspan='11'><h4><div align='center'>Data Tidak ada !!!</div></h4></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
                <?php
                echo $paginator;
                ?>
            </ul>
        </div>
    </section>
</div>
