<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?php echo base_url(); ?>peserta">Peserta SKB CPNS Kejaksaan RI 2021</a>
                <div class="span6 pull-right">
                    <?php echo form_open("peserta/cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span2" name="cari" placeholder="">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Pencarian</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <div align="center">
            <ul>
                <strong><?php
                    echo $result_count;
                    ?></strong>
            </ul>
        </div>
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th width="10%"><div align="center">No.</div></th>
                    <th width="60%">Peserta</th>
                    <th width="30%"><div align="center">Action</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data_peserta->result_array())) {
                    $no = $tot + 1;
                    foreach ($data_peserta->result_array() as $dp) {
                        ?>
                        <tr>
                            <td width="10%"><div align="center"><?php echo $no; ?>.</div></td>
                            <td width="60%"><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                                <?php echo $dp['no_peserta']; ?><br />
                                <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br /><?php echo $dp['satker']; ?><br /><?php echo $dp['agama']; ?><br /><?php echo $dp['tempat_lahir_ktp']; ?>, <?php echo datedoank($dp['tgl_lahir_ktp']); ?> (<?php
                                $lahir = new DateTime($dp['tgl_lahir_ktp']);
                                $today = new DateTime();
                                $umur = $today->diff($lahir);
                                echo $umur->y;
                                echo " Tahun, ";
                                echo $umur->m;
                                echo " Bulan, dan ";
                                echo $umur->d;
                                echo " Hari";
                                ?>)<br /><?php echo $dp['lembaga_pendidikan']; ?>
                            </td>
                            <td width="30%">
                                <div align="center">
                                    <form action="<?php echo base_url(); ?>peserta/detail/<?php echo $dp['id_peserta']; ?>" method="post">
                                        <input class="btn btn-xs btn-info" type="submit" value="Lihat Detail" />
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "<tr><td colspan='3'><h4><div align='center'>Data Tidak ada !!!</div></h4></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
<?php
echo $paginator;
?>
            </ul>
        </div>
    </section>
</div>
