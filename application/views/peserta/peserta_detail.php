<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Data Pelamar CPNS 2021</a>
                <div class="span6 pull-right">
                    <?php echo form_open("dashboard_admin/cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span3" name="cari" placeholder="Masukkan kata kunci pencarian">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Cari Data Pelamar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <form class="form-horizontal">
            <div class="control-group">
                <div class="span3"><strong>NAMA_KTP</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="nip" id="nip" value="<?php echo $nama_ktp; ?>" placeholder="NIP">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>NIK</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="nip_lama" id="nip_lama" value="<?php echo $nik; ?>" placeholder="NIP Lama">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>NOMOR PESERTA</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="no_kartu_pegawai" id="no_kartu_pegawai" value="<?php echo $no_peserta; ?>" placeholder="Nomor Kartu Pegawai">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>TEMPAT LAHIR</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="tempat_lahir_ktp" id="tempat_lahir_ktp" value="<?php echo $tempat_lahir_ktp; ?>" placeholder="Tempat Lahir">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>TANGGAL LAHIR</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="tgl_lahir_ktp" id="tgl_lahir_ktp" value="<?php echo datedoank($tgl_lahir_ktp); ?>" placeholder="Tanggal Lahir">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>UMUR</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="umur" id="umur" value="<?php
                    $lahir = new DateTime($tgl_lahir_ktp);
                    $today = new DateTime();
                    $umur = $today->diff($lahir);
                    echo $umur->y;
                    echo " Tahun, ";
                    echo $umur->m;
                    echo " Bulan, dan ";
                    echo $umur->d;
                    echo " Hari";
                    ?>" placeholder="Umur">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>JENIS KELAMIN</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="nama_pegawai" id="nama_pegawai" value="<?php echo $jenis_kelamin; ?>" placeholder="Nama Pegawai">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>JABATAN</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="tempat_lahir" id="tempat_lahir" value="<?php echo $jabatan_nama; ?>" placeholder="Tempat Lahir">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>JENIS FORMASI</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="no_npwp" id="no_npwp" value="<?php echo $jenis_formasi; ?>" placeholder="Nomor NPWP">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>LOKASI UJIAN</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="kartu_askes_pegawai" id="kartu_askes_pegawai" value="<?php echo $satker; ?>" placeholder="Kartu Askes Pegawai">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>LEMBAGA PENDIDIKAN</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="tanggal_lahir" id="tanggal_lahir" value="<?php echo $lembaga_pendidikan; ?>" placeholder="Tanggal Lahir">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>IPK / NILAI</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="ipk_nilai" id="tempat_lahir" value="<?php echo $ipk_nilai; ?>" placeholder="IPK / Nilai">
                </div>
            </div>
        </form> 
    </section>
</div>

<div class="well" align="center">
    <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
</div>
