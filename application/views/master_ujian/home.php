<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Master Ujian Praktek Kerja</a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li><a href="<?php echo base_url(); ?>master/tambah" class="small-box"><i class="icon-plus-sign icon-white"></i> Tambah Ujian</a></li>
                    </ul>
                </div>
                <div class="span6 pull-right">
                    <?php echo form_open('master_eselon/cari', 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span3" name="cari" placeholder="Masukkan kata kunci pencarian">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Cari Data</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th><div align="center">No.</div></th>
                    <th><div align="center">Judul</div></th>
                    <th><div align="center">Keterangan</div></th>
                    <th><div align="center">Dibuat</div></th>
                    <th><div align="center">Diubah</div></th>
                    <th><div align="center">File</div></th>
                    <th><div align="center">Aksi</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($master_ujian->result_array())) {
                    $no = $tot + 1;
                    foreach ($master_ujian->result_array() as $dp) {
                        ?>
                        <tr>
                            <td><div align="center"><?php echo $no; ?></div></td>
                            <td><div align="center"><?php echo $dp['judul']; ?></div></td>
                            <td><div align="center"><?php echo $dp['keterangan']; ?></div></td>
                            <td><div align="center"><?php echo datetimes($dp['create_time']); ?></div></td>
                            <td><div align="center">
                                    <?php
                                    if ($dp['update_time'] == '') {
                                        echo '-';
                                    } else {
                                        echo datetimes($dp['update_time']);
                                    }
                                    ?>
                                </div></td>
                            <td>
                                <div align="center">
                                    <embed type="application/pdf" src="<?php echo base_url(); ?>upload/master_ujian/<?php echo $dp['file']; ?>" width="600" height="400"></embed>
                                </div>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-small small-box" href="<?php echo base_url(); ?>master/detail/<?php echo $dp['id_master_ujian']; ?>"><i class="icon-ok-circle"></i> Lihat Detail</a>
                                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>master/edit/<?php echo $dp['id_master_ujian']; ?>" class="small-box"><i class="icon-pencil"></i> Edit Data</a></li>
                                        <li><a href="<?php echo base_url(); ?>master/hapus/<?php echo $dp['id_master_ujian']; ?>" onClick="return confirm('Anda yakin..???');"><i class="icon-trash"></i> Hapus Data</a></li>
                                    </ul>
                                </div><!-- /btn-group -->
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "<tr><td colspan='6'><h4><div align='center'>Data Tidak ada !!!</div></h4></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
                <?php
                echo $paginator;
                ?>
            </ul>
        </div>

    </section>
</div>
