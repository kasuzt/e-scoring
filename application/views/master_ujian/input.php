<div class="well">
    <?php if (validation_errors()) { ?>
        <div class="alert alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Terjadi Kesalahan!</h4>
            <?php echo validation_errors(); ?>
        </div>
    <?php } ?>
    <?php echo form_open_multipart('master/simpan', 'class="form-horizontal"'); ?>
    <div class="control-group">
        <legend>Master Ujian</legend>
        <label class="control-label" for="nama_eselon">Judul Ujian</label>
        <div class="controls">
            <input type="text" class="span6" name="judul" id="judul" value="<?php echo $judul; ?>" placeholder="Judul Ujian">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="keterangan">Keterangan</label>
        <div class="controls">
            <?php
            if ($st == "tambah") {
                ?>
                <textarea class="span6" style="outline:none; resize:none;" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo set_value('keterangan'); ?></textarea>
                <?php
            } else {
                ?>
                <textarea class="span6" style="outline:none; resize:none;" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="nip">Upload File</label>
        <div class="controls">
            <input type="file" class="span6" name="userfile" id="userfile" placeholder="Upload File"><embed type="application/pdf" src="<?php echo base_url(); ?>upload/master_ujian/<?php echo $file; ?>" width="600" height="400"></embed>
        </div>
    </div>
    <input type="hidden" name="id_param" value="<?php echo $id_param; ?>">
    <input type="hidden" name="st" value="<?php echo $st; ?>">
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary">Simpan Data</button>
            <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
        </div>
    </div>
    <?php echo form_close(); ?>
</div>   
