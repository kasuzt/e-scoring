<div class="well">
    <?php echo form_open('master/simpan', 'class="form-horizontal"'); ?>
    <div class="control-group">
        <legend>Master Ujian</legend>
        <label class="control-label" for="judul">Judul</label>
        <div class="controls">
            <input type="text" class="span" name="judul" id="judul" value="<?php echo $judul; ?>" placeholder="Judul" disabled>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="keterangan">Keterangan</label>
        <div class="controls">
            <textarea class="span6" style="outline:none; resize:none;" name="keterangan" id="keterangan" placeholder="Keterangan" disabled><?php echo $keterangan; ?></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="nip">File</label>
        <div class="controls">
            <embed type="application/pdf" src="<?php echo base_url(); ?>upload/master_ujian/<?php echo $file; ?>" width="600" height="400"></embed>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="create_time">Dibuat</label>
        <div class="controls">
            <input type="text" class="span" name="create_time" id="create_time" value="<?php echo datetimes($create_time); ?>" placeholder="Dibuat" disabled>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="update_time">Diubah</label>
        <div class="controls">
            <input type="text" class="span" name="update_time" id="update_time" value="<?php
            if ($update_time == '') {
                echo '-';
            } else {
                echo datetimes($update_time);
            }
            ?>" placeholder="Diubah" disabled>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
