<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?php echo base_url(); ?>manage_user">Manajemen User</a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li><a href="<?php echo base_url(); ?>manage_user/tambah" class="small-box"><i class="icon-plus-sign icon-white"></i> Tambah User</a></li>
                    </ul>
                </div>
                <div class="span6 pull-right">
                    <?php echo form_open("manage_user/cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span2" name="cari" placeholder="">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Pencarian</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <div align="center">
            <ul>
                <strong><?php echo $result_count; ?></strong>
            </ul>
        </div>
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th><div align="center">No.</div></th>
                    <th><div align="center">Username</div></th>
                    <th><div align="center">Nama Lengkap</div></th>
                    <th><div align="center">Level</div></th>
                    <th><div align="center">Satuan Kerja</div></th>
                    <th><div align="center">Aksi</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = $tot + 1;
                foreach ($status_pegawai->result_array() as $dp) {
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $dp['username']; ?></td>
                        <td><?php echo $dp['nama_lengkap']; ?></td>
                        <td>
                            <?php
                            if ($dp['stts'] == "administrator") {
                                echo 'Super Administrator';
                            } else if ($dp['stts'] == "pewawancara") {
                                echo 'Pewawancara';
                            } else if ($dp['stts'] == "panitia_pusat") {
                                echo 'Panitia Pusat';
                            } else if ($dp['stts'] == "panitia_daerah") {
                                echo 'Panitia Daerah';
                            }
                            ?>
                        </td>
                        <td><?php echo $dp['satker']; ?></td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-small small-box" href="<?php echo base_url(); ?>manage_user/detail/<?php echo $dp['id_user_login']; ?>"><i class="icon-ok-circle"></i> Lihat Detail</a>
                                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url(); ?>manage_user/edit/<?php echo $dp['id_user_login']; ?>" class="small-box"><i class="icon-pencil"></i> Edit Data</a></li>
                                    <li><a href="<?php echo base_url(); ?>manage_user/hapus/<?php echo $dp['id_user_login']; ?>" onClick="return confirm('Anda yakin..???');"><i class="icon-trash"></i> Hapus Data</a></li>
                                </ul>
                            </div><!-- /btn-group -->
                        </td>
                    </tr>
                    <?php
                    $no++;
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
                <?php
                echo $paginator;
                ?>
            </ul>
        </div>
    </section>
</div>
