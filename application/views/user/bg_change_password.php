<section id="data-keluarga">
    <div class="well">
        <?php echo $this->session->flashdata('pass'); ?>
        <?php if (validation_errors()) { ?>
            <div class="alert alert-block">
                <button type="button" class="close" data-dismiss="alert">�</button>
                <h4>Terjadi Kesalahan!</h4>
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>
        <div class="tabbable tabs-left">
            <?php
            if ($this->session->userdata("tab_a") == "" && $this->session->userdata("tab_b") == "") {
                $set['tab_a'] = "active";
                $this->session->set_userdata($set);
            }
            $a = $this->session->userdata("tab_a");
            $b = $this->session->userdata("tab_b");
            ?>
            <ul class="nav nav-tabs">
                <li class="<?php echo $a; ?>"><a href="#lA" data-toggle="tab">Pengaturan Kata Sandi</a></li>
                <li class="<?php echo $b; ?>"><a href="#lB" data-toggle="tab">Pengaturan Nama Lengkap</a></li>
                <li><a href="<?php echo base_url(); ?>app/logout">Keluar</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane <?php echo $a; ?>" id="lA">
                    <h4>Pengaturan Kata Sandi</h4>
                    <?php echo form_open('app/save_pass'); ?>
                    <div class="control-group">
                        <label class="control-label" for="pass_lama">Nama Pengguna</label>
                        <div class="controls">
                            <input type="text" value="<?php echo $this->session->userdata('username'); ?>" class="span4" name="username" id="username" placeholder="Nama Pengguna" readonly="true">
                        </div>
                        <label class="control-label" for="pass_lama">Kata Sandi Lama</label>
                        <div class="controls">
                            <input type="password" class="span4" name="pass_lama" id="pass_lama" placeholder="Kata Sandi Lama">
                        </div>
                        <label class="control-label" for="pass_lama">Kata Sandi Baru</label>
                        <div class="controls">
                            <input type="password" class="span4" name="pass_baru" id="pass_baru" placeholder="Kata Sandi Baru">
                        </div>
                        <label class="control-label" for="pass_lama">Ulangi Kata Sandi Baru</label>
                        <div class="controls">
                            <input type="password" class="span4" name="ulangi_pass_baru" id="ulangi_pass_baru" placeholder="Ulangi Kata Sandi Baru">
                        </div>
                        <div class="controls">
                            <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" class="span4" name="id_user" id="id_user" placeholder="Nama Pengguna" readonly="true">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Simpan Data</button>
                            <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="tab-pane <?php echo $b; ?>" id="lB">
                    <h4>Pengaturan Nama Lengkap</h4>
                    <?php echo form_open('app/save_name'); ?>
                    <div class="control-group">
                        <label class="control-label" for="pass_lama">Nama Lengkap</label>
                        <div class="controls">
                            <input type="text" value="<?php echo $this->session->userdata('nama'); ?>" class="span4" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap">
                        </div>
                        <div class="controls">
                            <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" class="span4" name="id_user" id="id_user" placeholder="Nama Lengkap" readonly="true">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Simpan Data</button>
                            <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div> <!-- /tabbable -->
    </div>
</section>
