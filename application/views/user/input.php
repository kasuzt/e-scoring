<div class="well">
    <?php echo $this->session->flashdata('pass'); ?>
    <?php if (validation_errors()) { ?>
        <div class="alert alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Terjadi Kesalahan!</h4>
            <?php echo validation_errors(); ?>
        </div>
    <?php } ?>
    <?php echo form_open('manage_user/simpan', 'class="form-horizontal"'); ?>

    <div class="control-group">
        <legend>Manajemen User</legend>
        <label class="control-label" for="nama_lengkap">Nama Lengkap</label>
        <div class="controls">
            <input type="text" class="span" name="nama_lengkap" id="nama_lengkap" value="<?php echo $nama_lengkap; ?>" placeholder="Nama Lengkap">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="username">Nama Pengguna</label>
        <div class="controls">
            <input type="text" class="span" name="username" id="username" value="<?php echo $username; ?>" <?php
            if ($st == "edit") {
                echo 'readonly="true"';
            }
            ?> placeholder="Nama Pengguna">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="password">Kata Sandi</label>
        <div class="controls">
            <input type="password" class="span" name="password" id="password" placeholder="Kata Sandi">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="kode_satker">Satuan Kerja</label>
        <div class="controls">
            <select class="chzn-select" style="width:300px;" tabindex="2" name="kode_satker" id="kode_satker">
                <option value="">-- SILAHKAN PILIH SATUAN KERJA --</option>
                <option value="1" <?php
                if ($kode_satker == "1") {
                    echo "selected";
                }
                ?>>ACEH</option>
                <option value="2" <?php
                if ($kode_satker == "2") {
                    echo "selected";
                }
                ?>>SUMATERA UTARA</option>
                <option value="3" <?php
                if ($kode_satker == "3") {
                    echo "selected";
                }
                ?>>RIAU</option>
                <option value="4" <?php
                if ($kode_satker == "4") {
                    echo "selected";
                }
                ?>>SUMATERA BARAT</option>
                <option value="5" <?php
                if ($kode_satker == "5") {
                    echo "selected";
                }
                ?>>JAMBI</option>
                <option value="6" <?php
                if ($kode_satker == "6") {
                    echo "selected";
                }
                ?>>SUMATERA SELATAN</option>
                <option value="7" <?php
                if ($kode_satker == "7") {
                    echo "selected";
                }
                ?>>LAMPUNG</option>
                <option value="8" <?php
                if ($kode_satker == "8") {
                    echo "selected";
                }
                ?>>BENGKULU</option>
                <option value="10" <?php
                if ($kode_satker == "10") {
                    echo "selected";
                }
                ?>>JAWA BARAT</option>
                <option value="11" <?php
                if ($kode_satker == "11") {
                    echo "selected";
                }
                ?>>DI YOGYAKARTA</option>
                <option value="12" <?php
                if ($kode_satker == "12") {
                    echo "selected";
                }
                ?>>JAWA TENGAH</option>
                <option value="13" <?php
                if ($kode_satker == "13") {
                    echo "selected";
                }
                ?>>JAWA TIMUR</option>
                <option value="14" <?php
                if ($kode_satker == "14") {
                    echo "selected";
                }
                ?>>KALIMANTAN BARAT</option>
                <option value="15" <?php
                if ($kode_satker == "15") {
                    echo "selected";
                }
                ?>>KALIMANTAN TENGAH</option>
                <option value="16" <?php
                if ($kode_satker == "16") {
                    echo "selected";
                }
                ?>>KALIMANTAN SELATAN</option>
                <option value="17" <?php
                if ($kode_satker == "17") {
                    echo "selected";
                }
                ?>>KALIMANTAN TIMUR</option>
                <option value="18" <?php
                if ($kode_satker == "18") {
                    echo "selected";
                }
                ?>>SULAWESI UTARA</option>
                <option value="19" <?php
                if ($kode_satker == "19") {
                    echo "selected";
                }
                ?>>SULAWESI TENGGARA</option>
                <option value="20" <?php
                if ($kode_satker == "20") {
                    echo "selected";
                }
                ?>>SULAWESI TENGAH</option>
                <option value="21" <?php
                if ($kode_satker == "21") {
                    echo "selected";
                }
                ?>>SULAWESI SELATAN</option>
                <option value="22" <?php
                if ($kode_satker == "22") {
                    echo "selected";
                }
                ?>>BALI</option>
                <option value="23" <?php
                if ($kode_satker == "23") {
                    echo "selected";
                }
                ?>>NUSA TENGGARA BARAT</option>
                <option value="24" <?php
                if ($kode_satker == "24") {
                    echo "selected";
                }
                ?>>NUSA TENGGARA TIMUR</option>
                <option value="25" <?php
                if ($kode_satker == "25") {
                    echo "selected";
                }
                ?>>MALUKU</option>
                <option value="26" <?php
                if ($kode_satker == "26") {
                    echo "selected";
                }
                ?>>PAPUA</option>
                <option value="27" <?php
                if ($kode_satker == "27") {
                    echo "selected";
                }
                ?>>MALUKU UTARA</option>
                <option value="28" <?php
                if ($kode_satker == "28") {
                    echo "selected";
                }
                ?>>BANTEN</option>
                <option value="29" <?php
                if ($kode_satker == "29") {
                    echo "selected";
                }
                ?>>KEPULAUAN BANGKA BELITUNG</option>
                <option value="30" <?php
                if ($kode_satker == "30") {
                    echo "selected";
                }
                ?>>GORONTALO</option>
                <option value="31" <?php
                if ($kode_satker == "31") {
                    echo "selected";
                }
                ?>>KEPULAUAN RIAU</option>
                <option value="00" <?php
                if ($kode_satker == "00") {
                    echo "selected";
                }
                ?>>KEJAKSAAN AGUNG</option>
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="stts">Level</label>
        <div class="controls">
            <select class="chzn-select" style="width:300px;" tabindex="2" name="stts" id="stts">
                <option value="">-- SILAHKAN PILIH LEVEL --</option>
                <option value="administrator" <?php
                if ($stts == "administrator") {
                    echo "selected";
                }
                ?>>Administrator</option>
                <option value="pewawancara" <?php
                if ($stts == "pewawancara") {
                    echo "selected";
                }
                ?>>Pewawancara</option>
                <option value="panitia_pusat" <?php
                if ($stts == "panitia_pusat") {
                    echo "selected";
                }
                ?>>Panitia Pusat</option>
                <option value="panitia_daerah" <?php
                if ($stts == "panitia_daerah") {
                    echo "selected";
                }
                ?>>Panitia Daerah</option>
            </select>
        </div>
    </div>

    <input type="hidden" name="id_param" value="<?php echo $id_param; ?>">
    <input type="hidden" name="default_username" value="<?php echo $username; ?>">
    <input type="hidden" name="st" value="<?php echo $st; ?>">
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary">Simpan Data</button>
            <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
        </div>
    </div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
        $(".chzn-select").chosen();
    </script>
</div>    
