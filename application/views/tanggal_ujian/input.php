<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#"><?php echo $nama_ktp; ?> [<?php echo $jabatan_nama; ?> - <?php echo $jenis_formasi; ?>]</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <form class="form-horizontal">
        <div class="control-group">
            <div class="span3"><strong>NO. PESERTA</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="no_peserta" id="no_peserta" value="<?php echo $no_peserta; ?>" placeholder="NO. PESERTA">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>JENIS KELAMIN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="jenis_kelamin" id="jenis_kelamin" value="<?php echo $jenis_kelamin; ?>" placeholder="JENIS KELAMIN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LEMBAGA PENDIDIKAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="lembaga_pendidikan" id="lembaga_pendidikan" value="<?php echo $lembaga_pendidikan; ?>" placeholder="LEMBAGA PENDIDIKAN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LOKASI UJIAN</strong></div>
            <div class="span">:</div>
            <div class="span6">
                <input type="text" disabled="disabled" class="span6" name="satker" id="satker" value="<?php echo $satker; ?>" placeholder="LOKASI UJIAN">
            </div>
        </div>
    </form>

    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Formulir Tes Wawancara</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <?php if (validation_errors()) { ?>
        <div class="alert alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Terjadi Kesalahan!</h4>
            <?php echo validation_errors(); ?>
        </div>
    <?php } ?>

    <form action="<?php echo base_url(); ?>wawancara/simpan_nilai" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">

        <div class="control-group">
            <div class="span3"><strong>PENGETAHUAN UMUM</strong></div>
            <div class="span">:</div>
            <div class="span6" id="telo1">
                <input type="text" class="span1 validate[required] text-input formatMataUang" name="pengetahuan_umum" id="pengetahuan_umum" autocomplete="off">
                <label id="lbl_1"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>PENGETAHUAN AKADEMIK / SKRIPSI</strong></div>
            <div class="span">:</div>
            <div class="span6" id="telo2">
                <input type="text" class="span1 validate[required] text-input formatMataUang" name="pengetahuan_akademik" id="pengetahuan_akademik" autocomplete="off">
                <label id="lbl_2"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>MOTIVASI BEKERJA</strong></div>
            <div class="span">:</div>
            <div class="span6" id="telo3">
                <input type="text" class="span1 validate[required] text-input formatMataUang" name="motivasi_bekerja" id="motivasi_bekerja" autocomplete="off">
                <label id="lbl_3"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>PENGAMBILAN KEPUTUSAN</strong></div>
            <div class="span">:</div>
            <div class="span6" id="telo4">
                <input type="text" class="span1 validate[required] text-input formatMataUang" name="pengambilan_keputusan" id="pengambilan_keputusan" autocomplete="off">
                <label id="lbl_4"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>PENAMPILAN DAN SIKAP DIRI</strong></div>
            <div class="span">:</div>
            <div class="span6" id="telo5">
                <input type="text" class="span1 validate[required] text-input formatMataUang" name="penampilan_sikap" id="penampilan_sikap">
                <label id="lbl_5"></label>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>KEMAMPUAN VERBAL / CARA BICARA</strong></div>
            <div class="span">:</div>
            <div class="span6" id="telo6">
                <input type="text" class="span1 validate[required] text-input formatMataUang" name="kemampuan_verbal" id="kemampuan_verbal" autocomplete="off">
                <label id="lbl_6"></label>
            </div>
        </div>  

        <div class="control-group">
            <div class="controls" align="center">
                <button type="submit" class="btn btn-primary" onClick="return confirm('Anda yakin data nilai tes wawancara sudah benar ?');">Simpan Data</button>
                <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
            </div>
        </div>

        <input type="hidden" name="id_peserta" value="<?php echo $id_peserta; ?>">
        <script type="text/javascript">

        </script>

    </form>

</div>
<script type="text/javascript">
    jQuery.fn.ForceNumericOnly =
            function ()
            {
                return this.each(function ()
                {
                    $(this).keydown(function (event)
                    {
                        x = $(this).val($(this).val().replace(/[^\d].+/, ""));
                        if ((event.which < 48 || event.which > 57)) {
                            event.preventDefault();
                        }
                        return x;
                    });
                });
            };

    $('.formatMataUang').ForceNumericOnly();

    function declarenilai(n) {
        var res = "";
        if (n >= 61 && n <= 70) {
            res = "Cukup";
        } else if (n >= 71 && n <= 80) {
            res = "Sedang";
        } else if (n >= 81 && n <= 90) {
            res = "Baik";
        } else if (n >= 91) {
            res = "Sangat Baik";
        } else {
            res = "Dibawah 60";
        }
        return res;
    }

    $("#pengetahuan_umum").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {

            alert('Nilai harus diantara 61 s/d 90');
            $("#pengetahuan_umum").val('');
        } else {
            $("#lbl_1").text(declarenilai(nilai));
        }
    });

    $("#pengetahuan_akademik").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#pengetahuan_akademik").val('');
        } else {
            $("#lbl_2").text(declarenilai(nilai));
        }
    });

    $("#motivasi_bekerja").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#motivasi_bekerja").val('');
        } else {
            $("#lbl_3").text(declarenilai(nilai));
        }
    });

    $("#pengambilan_keputusan").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#pengambilan_keputusan").val('');
        } else {
            $("#lbl_4").text(declarenilai(nilai));
        }
    });

    $("#penampilan_sikap").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#penampilan_sikap").val('');
        } else {
            $("#lbl_5").text(declarenilai(nilai));
        }
    });

    $("#kemampuan_verbal").blur(function () {
        var nilai = $(this).val();
        if (nilai < 61 || nilai > 90) {
            alert('Nilai harus diantara 61 s/d 90');
            $("#kemampuan_verbal").val('');
        } else {
            $("#lbl_6").text(declarenilai(nilai));
        }
    });

</script>
