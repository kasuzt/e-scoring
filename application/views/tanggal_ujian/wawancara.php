<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?php echo base_url(); ?>tanggal_ujian/wawancara">Pelamar Ujian SKB Tes Wawancara</a>
                <div class="span6 pull-right">
                    <?php echo form_open("tanggal_ujian/wawancara_cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span2" name="cari" placeholder="">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Pencarian</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <?= $this->session->flashdata('notif') ?>
    <section>
        <div align="center">
            <ul>
                <strong><?php echo $result_count; ?></strong>
            </ul>
        </div>
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th><div align="center">No.</div></th>
                    <th><div align="center">Peserta</div></th>
                    <th><div align="center">Aksi</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data_peserta->result_array())) {
                    $no = $tot + 1;
                    foreach ($data_peserta->result_array() as $dp) {
                        ?>
                        <tr>
                            <td width="10%"><div align="center"><?php echo $no; ?>.</div></td>
                            <td width="60%"><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                                <?php echo $dp['no_peserta']; ?><br />
                                <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br />
                                <?php echo $dp['satker']; ?><br /><?php echo $dp['tempat_lahir_ktp']; ?>, <?php echo datedoank($dp['tgl_lahir_ktp']); ?> (<?php
                                $lahir = new DateTime($dp['tgl_lahir_ktp']);
                                $today = new DateTime();
                                $umur = $today->diff($lahir);
                                echo $umur->y;
                                echo " Tahun, ";
                                echo $umur->m;
                                echo " Bulan, dan ";
                                echo $umur->d;
                                echo " Hari";
                                ?>)<br /><?php echo $dp['lembaga_pendidikan']; ?>
                            </td>
                            <td width="30%">
                                <a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_edit<?php echo $dp['id_peserta']; ?>"> Tambah Tanggal Wawancara & Pewawancara</a>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "<tr><td colspan='3'><h4><div align='center'>Data Tidak ada !!!</div></h4></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
<?php
echo $paginator;
?>
            </ul>
        </div>
    </section>
</div>

<!-- ============ MODAL TAMBAH TANGGAL UJIAN WAWANCARA =============== -->
<?php
foreach ($data_peserta->result_array() as $dp):
    $id_peserta = $dp['id_peserta'];
    $nama_ktp = $dp['nama_ktp'];
    $no_peserta = $dp['no_peserta'];
    $jabatan_nama = $dp['jabatan_nama'];
    ?>
    <div class="modal fade" id="modal_edit<?php echo $id_peserta; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h3 class="modal-title" id="myModalLabel">Tambah Tanggal Wawancara & Pewawancara</h3>
                </div>
                <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>tanggal_ujian/wawancara_ubah" id="formID">
                    <div class="modal-body">

                        <div class="control-group">
                            <div class="span1"><strong>Nama KTP</strong></div>
                            <div class="span">:</div>
                            <div class="span3">
                                <input type="text" class="span4" name="nama_ktp" disabled="" value="<?php echo $nama_ktp; ?>">
                                <label id="lbl_1"></label>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="span1"><strong>No. Peserta</strong></div>
                            <div class="span">:</div>
                            <div class="span3">
                                <input type="text" class="span4" name="no_peserta" disabled="" value="<?php echo $no_peserta; ?>">
                                <label id="lbl_1"></label>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="span1"><strong>Jabatan</strong></div>
                            <div class="span">:</div>
                            <div class="span3">
                                <input type="text" class="span4" name="jabatan_nama" disabled="" value="<?php echo $jabatan_nama; ?>">
                                <label id="lbl_1"></label>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="span1"><strong>Tanggal Ujian</strong></div>
                            <div class="span">:</div>
                            <div class="span3">
                                <input type="text" class="span2" name="tgl_wawancara" id="date" required="">
                                <label id="lbl_1"></label>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="span1"><strong>Pewawancara</strong></div>
                            <div class="span">:</div>
                            <div class="span3" id="id_user">
                                <select class="form-control" name="id_user">
    <?php
    foreach ($user as $row) {
        echo '<option value="' . $row->id_user_login . '">' . $row->nama_lengkap . '</option>';
    }
    ?>
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="id_peserta" value="<?php echo $id_peserta; ?>">

                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-info">Simpan</button>
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL TAMBAH TANGGAL UJIAN WAWANCARA-->

<script src="<?php echo base_url() . 'asset/js/moment.js' ?>"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script type="text/javascript">
    $(document).ready(function () {
        var date_input = $('input[id="date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>
