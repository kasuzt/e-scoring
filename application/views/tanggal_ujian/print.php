<html>
    <head>
        <title>e-Scroring SKB CPNS Kejaksaan Republik Indonesia 1619</title>
    </head>
    <body>
        <style type="text/css">
            #customers {
                border-collapse: collapse;
                width: 100%;
            }

            #customers td, #customers th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #customers tr:nth-child(even){
                background-color: #f2f2f2;
            }

            #customers tr:hover {
                background-color: #ddd;
            }

            #customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
            }
            #customers tr:nth-child(even) {
                background-color: #f2f2f2;
            }
        </style>
        <table border="1" align="center" id="customers" width="100%">
            <tr>
                <th align="center">No.</th>
                <th align="center">Peserta</th>
                <th align="center">Ujian Komputer</th>
                <th align="center">Ujian Beladiri</th>
                <th align="center">Ujian Mengemudi</th>
            </tr>
            <?php
            if (!empty($data_peserta)) {
                $no = 1;
                foreach ($data_peserta->result() as $data) {
                    ?>
                    <tr>
                        <td><p align="center"><?php echo $no; ?>.</p></td>
                        <td>
                            <?php echo $data->nama_ktp; ?> [<?php echo $data->jenis_kelamin; ?>]<br/>
                            <?php echo $data->no_peserta; ?><br/>
                            <?php echo $data->jabatan_nama; ?> [<?php echo $data->jenis_formasi; ?>]<br/>
                            <?php echo $data->satker; ?>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->tgl_komputer != '') {
                                    echo datedoank($data->tgl_komputer);
                                } else {
                                    echo '-';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->tgl_beladiri != '') {
                                    echo datedoank($data->tgl_beladiri);
                                } else {
                                    echo '-';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->tgl_mengemudi != '') {
                                    echo datedoank($data->tgl_mengemudi);
                                } else {
                                    echo '-';
                                }
                                ?>
                            </p>
                        </td>
                    </tr>
                    <?php
                    $no++;
                }
            }
            ?>
        </table>

    </body>
</html>
