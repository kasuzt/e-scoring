<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Pelamar Ujian SKB</a>
                <div class="span6 pull-right">
                    <?php echo form_open("tanggal_ujian/cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span2" name="cari" placeholder="">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Pencarian</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <div align="center">
            <ul>
                <strong><?php echo $result_count; ?></strong>
            </ul>
        </div>
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th><div align="center">No.</div></th>
                    <th><div align="center">Peserta</div></th>
                    <th><div align="center">Lokasi Ujian</div></th>
                    <th><div align="center">Tgl Ujian Komputer</div></th>
                    <th><div align="center">Tgl Ujian Beladiri</div></th>
                    <th><div align="center">Tgl Ujian Mengemudi</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data_peserta->result_array())) {
                    $no = $tot + 1;
                    foreach ($data_peserta->result_array() as $dp) {
                        ?>
                        <tr>
                            <td><div align="center"><?php echo $no; ?>.</div></td>
                            <td><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                                <?php echo $dp['no_peserta']; ?><br />
                                <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br />
                                <?php echo $dp['satker']; ?>
                            </td>
                            <td><div align="center"><?php echo $dp['satker']; ?></div></td>
                            <td>
                                <div align="center">
                                    <?php
                                    if ($dp['tgl_komputer'] == '') {
                                        echo '-';
                                    } else {
                                        echo datedoank($dp['tgl_komputer']);
                                    }
                                    ?>
                                </div>
                            </td>
                            <td>
                                <div align="center">
                                    <?php
                                    if ($dp['tgl_beladiri'] == '') {
                                        echo '-';
                                    } else {
                                        echo datedoank($dp['tgl_beladiri']);
                                    }
                                    ?>
                                </div>
                            </td>
                            <td>
                                <div align="center">
                                    <?php
                                    if ($dp['tgl_mengemudi'] == '') {
                                        echo '-';
                                    } else {
                                        echo datedoank($dp['tgl_mengemudi']);
                                    }
                                    ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "<tr><td colspan='3'><h4><div align='center'>Data Tidak ada !!!</div></h4></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
                <?php
                echo $paginator;
                ?>
            </ul>
        </div>
    </section>
    <div align="center"><a class="btn btn-small" href="<?php echo base_url(); ?>tanggal_ujian/cetak" target="_blank"><i class="icon-print"></i> Cetak Hasil</a></div>
</div>
