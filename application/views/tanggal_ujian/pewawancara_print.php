<html>
    <head>
        <title>e-Scroring SKB CPNS Kejaksaan Republik Indonesia 1619</title>
    </head>    
    <style type="text/css">
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 16px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
    </style>
    <body>
        <div align="center">
            <h2>
                <strong>
                    <span class="style1">KEJAKSAAN AGUNG REPUBLIK INDONESIA<br>Panitia Penerimaan CPNS Kejaksaan Republik Indonesia 2021<br>
                        <br />Daftar Nama Peserta Wawancara<br/><?php echo $nama_lengkap; ?>
                    </span>
                </strong>
            </h2>
            <p>&nbsp;</p>
        </div>
        <table border="1" align="center" width="100%">
            <tr>
                <th align="center">No.</th>
                <th align="center">Peserta</th>
                <th align="center">Tanggal Ujian</th>
                <th align="center">Keterangan</th>
            </tr>
            <?php
            if (!empty($data_peserta)) {
                $no = 1;
                foreach ($data_peserta->result() as $data) {
                    ?>
                    <tr>
                        <td><p align="center"><?php echo $no; ?>.</p></td>
                        <td>
                            <?php echo $data->nama_ktp; ?> [<?php echo $data->jenis_kelamin; ?>]<br/>
                            <?php echo $data->no_peserta; ?><br/>
                            <?php echo $data->jabatan_nama; ?> [<?php echo $data->jenis_formasi; ?>]<br/><?php echo $data->satker; ?>
                        </td>
                        <td><p align="center"><?php echo datedoank($data->tgl_wawancara); ?></p></td>
                        <td></td>
                    </tr>
                    <?php
                    $no++;
                }
            }
            ?>
        </table>
    </body>
</html>
