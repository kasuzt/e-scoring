<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Cetak Pewawancara Beserta Pesertanya</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <legend>Cetak Pewawancara Beserta Pesertanya</legend>
        <form action="<?php echo base_url(); ?>tanggal_ujian/cetak_pewawancara" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div class="control-group">
                <div class="span2"><strong>Pilih Pewawancara</strong></div>
                <div class="span">:</div>
                <div class="span6" id="user">
                    <select class="form-control span4" name="user">
                        <?php
                        foreach ($user_wawancara as $row) {
                            echo '<option value="' . $row->id_user_login . '">' . $row->nama_lengkap . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary" formtarget="_blank">Cetak</button>
                </div>
            </div>
        </form>
    </section>
</div>
