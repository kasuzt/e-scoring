<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?php echo base_url(); ?>"><?php echo $judul_pendek; ?></a>
            <?php if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "administrator") { ?>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> Ujian<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>wawancara"><i class="fa fa-microphone"></i> Wawancara</a></li>
                                <li><a href="<?php echo base_url(); ?>praktek_kerja"><i class="fas fa-desktop"></i> Praktek Kerja</a></li>
                                <li><a href="<?php echo base_url(); ?>beladiri"><i class="icon-warning-sign"></i> Beladiri</a></li>
                                <li><a href="<?php echo base_url(); ?>samapta"><i class="icon-road"></i> Kesamaptaan</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-envelope icon-white"></i> Hasil<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>nilai/wawancara"><i class="fa fa-microphone"></i> Wawancara</a></li>
                                <li><a href="<?php echo base_url(); ?>nilai/praktek_kerja"><i class="fas fa-desktop icon-white"></i> Praktek Kerja</a></li>
                                <li><a href="<?php echo base_url(); ?>nilai/beladiri"><i class="icon-warning-sign"></i> Beladiri</a></li>
                                <li><a href="<?php echo base_url(); ?>nilai/samapta"><i class="icon-road"></i> Kesamaptaan</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-calendar icon-white"></i> Tanggal Ujian<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/wawancara"><i class="fa fa-microphone"></i> Wawancara</a></li>
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/praktek_kerja"><i class="fa fa-desktop"></i> Praktek Kerja</a></li>
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/beladiri"><i class="icon-warning-sign"></i> Beladiri</a></li>
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/samapta"><i class="icon-road"></i>  Kesamaptaan</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>peserta"><i class="icon-user icon-white"></i> Peserta</a></li>
                        <li><a href="<?php echo base_url(); ?>master"><i class="icon-calendar icon-white"></i> Master</a></li>
                    </ul>
                    <div class="btn-group pull-right">
                        <button class="btn btn-primary"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('nama'); ?></button>
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>app/change_password"><i class="icon-wrench"></i> Pengaturan Akun</a></li>
                            <li><a href="<?php echo base_url(); ?>manage_user"><i class="icon-user"></i> Manajemen User</a></li>
                            <li><a href="<?php echo base_url(); ?>app/logout"><i class="icon-off"></i> Keluar</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            <?php } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "pewawancara") { ?>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li><a href="<?php echo base_url(); ?>wawancara"><i class="icon-book icon-white"></i> Wawancara</a></li>
                    </ul>
                    <div class="btn-group pull-right">
                        <button class="btn btn-primary"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('nama'); ?></button>
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>app/change_password"><i class="icon-wrench"></i> Pengaturan Akun</a></li>
                            <li><a href="<?php echo base_url(); ?>app/logout"><i class="icon-off"></i> Keluar</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            <?php } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "presentasi") { ?>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li><a href="<?php echo base_url(); ?>presentasi"><i class="icon-book icon-white"></i> Presentasi</a></li>
                    </ul>
                    <div class="btn-group pull-right">
                        <button class="btn btn-primary"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('nama'); ?></button>
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>app/change_password"><i class="icon-wrench"></i> Pengaturan Akun</a></li>
                            <li><a href="<?php echo base_url(); ?>app/logout"><i class="icon-off"></i> Keluar</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            <?php } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_pusat") { ?>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li><a href="<?php echo base_url(); ?>praktek_kerja"><i class="fas fa-desktop icon-white icon-white"></i> Praktek Kerja</a></li>
                        <li><a href="<?php echo base_url(); ?>beladiri"><i class="icon-warning-sign icon-white"></i> Beladiri</a></li>
                        <li><a href="<?php echo base_url(); ?>samapta"><i class="icon-road icon-white"></i> Kesamaptaan</a></li>
                    </ul>
                    <div class="btn-group pull-right">
                        <button class="btn btn-primary"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('nama'); ?></button>
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>app/change_password"><i class="icon-wrench"></i> Pengaturan Akun</a></li>
                            <li><a href="<?php echo base_url(); ?>app/logout"><i class="icon-off"></i> Keluar</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            <?php } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "panitia_daerah") { ?>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li><a href="<?php echo base_url(); ?>praktek_kerja"><i class="fas fa-desktop icon-white"></i> Praktek Kerja</a></li>
                        <li><a href="<?php echo base_url(); ?>beladiri"><i class="icon-warning-sign icon-white"></i> Beladiri</a></li>
                        <li><a href="<?php echo base_url(); ?>samapta"><i class="icon-road icon-white"></i> Kesamaptaan</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-calendar icon-white"></i> Tanggal Ujian<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/praktek_kerja"><i class="fa fa-desktop"></i> Praktek Kerja</a></li>
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/beladiri"><i class="icon-warning-sign"></i> Beladiri</a></li>
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/samapta"><i class="icon-road"></i>  Kesamaptaan</a></li>
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian"><i class="icon-print"></i> Cetak Ujian</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="btn-group pull-right">
                        <button class="btn btn-primary"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('nama'); ?></button>
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>app/change_password"><i class="icon-wrench"></i> Pengaturan Akun</a></li>
                            <li><a href="<?php echo base_url(); ?>app/logout"><i class="icon-off"></i> Keluar</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            <?php } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('stts') == "koni_pusat") { ?>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li><a href="<?php echo base_url(); ?>beladiri"><i class="icon-warning-sign icon-white"></i> Beladiri</a></li>
                        <li><a href="<?php echo base_url(); ?>samapta"><i class="icon-road icon-white"></i> Kesamaptaan</a></li>
                        <li class="dropdown">
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/praktek_kerja"><i class="fa fa-desktop"></i> Praktek Kerja</a></li>
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/beladiri"><i class="icon-warning-sign"></i> Beladiri</a></li>
                                <li><a href="<?php echo base_url(); ?>tanggal_ujian/samapta"><i class="icon-road"></i>  Kesamaptaan</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-envelope icon-white"></i> Hasil<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>nilai/beladiri"><i class="icon-warning-sign"></i> Beladiri</a></li>
                                <li><a href="<?php echo base_url(); ?>nilai/samapta"><i class="icon-road"></i> Kesamaptaan</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="btn-group pull-right">
                        <button class="btn btn-primary"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('nama'); ?></button>
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>app/change_password"><i class="icon-wrench"></i> Pengaturan Akun</a></li>
                            <li><a href="<?php echo base_url(); ?>app/logout"><i class="icon-off"></i> Keluar</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            <?php } else { ?>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li><a href="<?php echo base_url(); ?>ujian"><i class="fas fa-desktop icon-white"></i> Ujian Praktek Kerja</a></li>
                    </ul>
                    <div class="btn-group pull-right">
                        <button class="btn btn-primary"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('nama'); ?></button>
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>app/logout"><i class="icon-off"></i> Keluar</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            <?php } ?>
        </div>
    </div>
</div>
