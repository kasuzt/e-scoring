<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $judul_lengkap . ' - ' . $instansi; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Sistem Penilaian SKB CPNS Kejaksaan RI 2021">
        <meta name="author" content="d35yk4">
        <link rel="shortcut icon" href="<?= base_url(); ?>asset/img/favicon.png" />
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>asset/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>asset/css/docs.css" rel="stylesheet" />

        <script src="<?php echo base_url(); ?>asset/js/jquery-latest.js"></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>asset/js/application.js"></script>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap-tooltip.js"></script>
        <script src="<?php echo base_url(); ?>asset/js/number.js"></script>
        <script src='<?php echo base_url(); ?>asset/js/a076d05399.js'></script>

        <!-- validationEngine -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset/validation/validationEngine.jquery.css" type="text/css"/>
        <script src="<?php echo base_url(); ?>asset/validation/jquery.validationEngine-id.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php echo base_url(); ?>asset/validation/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <script>
            jQuery(document).ready(function () {
                // binds form submission and fields to the validation engine
                jQuery("#formID").validationEngine();
            });
        </script>
        <!-- // validationEngine -->

    </head>

    <body>
        <?php $this->load->view('template/navbar'); ?>
        <div class="container">
            <?php // $this->load->view('template/instansi'); ?>
            <?php $this->load->view($main); ?>
            <?php $this->load->view('template/footer'); ?>
        </div>
        <!-- /container -->
    </body>
</html>
