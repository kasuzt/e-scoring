<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Data Pelamar CPNS 2021</a>
                <div class="span6 pull-right">
                    <?php echo form_open("dashboard_admin/cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span3" name="cari" placeholder="Masukkan kata kunci pencarian">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Cari Data Pelamar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <form class="form-horizontal">
            <div class="control-group">
                <div class="span3"><strong>NAMA_KTP</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="nip" id="nip" value="<?php echo $NAMA_KTP; ?>" placeholder="NIP">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>NIK</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="nip_lama" id="nip_lama" value="<?php echo $NIK; ?>" placeholder="NIP Lama">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>NO_PESERTA</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="no_kartu_pegawai" id="no_kartu_pegawai" value="<?php echo $NO_PESERTA; ?>" placeholder="Nomor Kartu Pegawai">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>JENIS_KELAMIN</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="nama_pegawai" id="nama_pegawai" value="<?php echo $JENIS_KELAMIN; ?>" placeholder="Nama Pegawai">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>JABATAN_NAMA</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="tempat_lahir" id="tempat_lahir" value="<?php echo $JABATAN_NAMA; ?>" placeholder="Tempat Lahir">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>JENIS_FORMASI</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="no_npwp" id="no_npwp" value="<?php echo $JENIS_FORMASI; ?>" placeholder="Nomor NPWP">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>LOKASI_UJIAN_NAMA</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="kartu_askes_pegawai" id="kartu_askes_pegawai" value="<?php echo $LOKASI_UJIAN_NAMA; ?>" placeholder="Kartu Askes Pegawai">
                </div>
            </div>
            <div class="control-group">
                <div class="span3"><strong>LEMBAGA_PENDIDIKAN</strong></div>
                <div class="span">:</div>
                <div class="span6">
                    <input type="text" disabled="disabled" class="span6" name="tanggal_lahir" id="tanggal_lahir" value="<?php echo $LEMBAGA_PENDIDIKAN; ?>" placeholder="Tanggal Lahir">
                </div>
            </div>
        </form> 
    </section>
</div>

<div class="well" align="center">
    <input type="button" class="btn btn-default" value="Kembali" onClick="javascript: history.go(-1)" />
</div>
