<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#"><?php echo $nama_ktp; ?> [<?php echo $jabatan_nama; ?> - <?php echo $jenis_formasi; ?>]</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <form class="form-horizontal">
        <div class="control-group">
            <div class="span3"><strong>FOTO</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <img src="<?php echo base_url(); ?>uploads/<?php echo $no_peserta; ?> <?php echo $nama_ktp; ?>.jpg" width="200" />
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>NO. PESERTA</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="text" disabled="disabled" class="span7" name="no_peserta" id="no_peserta" value="<?php echo $no_peserta; ?>" placeholder="NO. PESERTA">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>JENIS KELAMIN</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="text" disabled="disabled" class="span7" name="jenis_kelamin" id="jenis_kelamin" value="<?php echo $jenis_kelamin; ?>" placeholder="JENIS KELAMIN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LEMBAGA PENDIDIKAN</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="text" disabled="disabled" class="span7" name="lembaga_pendidikan" id="lembaga_pendidikan" value="<?php echo $lembaga_pendidikan; ?>" placeholder="LEMBAGA PENDIDIKAN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LOKASI UJIAN</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="text" disabled="disabled" class="span7" name="satker" id="satker" value="<?php echo $satker; ?>" placeholder="LOKASI UJIAN">
            </div>
        </div>
    </form>

    <?php
    if (isset($error)) {
        echo "ERROR UPLOAD : <br/>";
        print_r($error);
        echo "<hr/>";
    }
    ?>

    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Upload Jawaban Anda Di Bawah Ini !</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <form action="<?php echo base_url(); ?>ujian/upload_simpan" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">

        <div class="control-group">
            <div class="span3"><strong>UPLOAD FILE DOC / DOCX</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="file" class="span7" name="berkas[]">
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>KETERANGAN FILE</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <textarea name="keterangan_berkas[]" class="span7" ></textarea>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>UPLOAD FILE XLS / XLSX</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="file" class="span7" name="berkas[]">
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>KETERANGAN FILE</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <textarea name="keterangan_berkas[]" class="span7" ></textarea>
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>UPLOAD FILE PPT / PPTX</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="file" class="span7" name="berkas[]">
            </div>
        </div>

        <div class="control-group">
            <div class="span3"><strong>KETERANGAN FILE</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <textarea name="keterangan_berkas[]" class="span7" ></textarea>
            </div>
        </div>

        <div class="control-group">
            <div class="controls" align="center">
                <button type="submit" class="btn btn-primary" onClick="return confirm('Anda Yakin File Yang Di Upload Sudah Benar ?');">Kirim Jawaban</button>
            </div>
        </div>

        <input type="hidden" name="no_peserta" value="<?php echo $no_peserta; ?>">
        <script type="text/javascript">

        </script>

    </form>

</div>
