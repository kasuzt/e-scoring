<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#"><?php echo $nama_ktp; ?> [<?php echo $jabatan_nama; ?> - <?php echo $jenis_formasi; ?>]</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <form class="form-horizontal">
        <div class="control-group">
            <div class="span3"><strong>FOTO</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <img src="<?php echo base_url(); ?>uploads/<?php echo $no_peserta; ?> <?php echo $nama_ktp; ?>.jpg" width="200" />
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>NO. PESERTA</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="text" disabled="disabled" class="span7" name="no_peserta" id="no_peserta" value="<?php echo $no_peserta; ?>" placeholder="NO. PESERTA">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>JENIS KELAMIN</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="text" disabled="disabled" class="span7" name="jenis_kelamin" id="jenis_kelamin" value="<?php echo $jenis_kelamin; ?>" placeholder="JENIS KELAMIN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LEMBAGA PENDIDIKAN</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="text" disabled="disabled" class="span7" name="lembaga_pendidikan" id="lembaga_pendidikan" value="<?php echo $lembaga_pendidikan; ?>" placeholder="LEMBAGA PENDIDIKAN">
            </div>
        </div>
        <div class="control-group">
            <div class="span3"><strong>LOKASI UJIAN</strong></div>
            <div class="span">:</div>
            <div class="span7">
                <input type="text" disabled="disabled" class="span7" name="satker" id="satker" value="<?php echo $satker; ?>" placeholder="LOKASI UJIAN">
            </div>
        </div>
    </form>

    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">Kerjakan Soal Di Bawah Ini !</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th><div align="center">No.</div></th>
                <th><div align="center">Judul</div></th>
                <th><div align="center">Keterangan</div></th>
                <th><div align="center">File</div></th>
                <th><div align="center">Aksi</div></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><div align="center">1.</div></td>
                <td><div align="center"><?php echo $judul; ?></div></td>
                <td><div align="center"><?php echo $keterangan; ?></div></td>
                <td>
                    <div align="center">
                        <embed type="application/pdf" src="<?php echo base_url(); ?>upload/master_ujian/<?php echo $file; ?>" width="600" height="400"></embed>
                    </div>
                </td>
                <td>
                    <div align="center">
                        <a class="btn btn-small" href="<?php echo base_url(); ?>ujian/selesai_ujian/<?php echo encrypt_url($no_peserta); ?>"><i class="icon-pencil"></i> Selesai Ujian</a>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <?php if (validation_errors()) { ?>
        <div class="alert alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Terjadi Kesalahan!</h4>
            <?php echo validation_errors(); ?>
        </div>
    <?php } ?>

</div>
