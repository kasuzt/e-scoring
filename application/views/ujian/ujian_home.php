<div class="well">
   <div class="navbar navbar-inverse">
      <div class="navbar-inner">
         <div class="container">
            <a class="brand" href="<?php echo base_url(); ?>peserta">SKB CPNS Kejaksaan RI 2021</a>
         </div>
      </div>
      <!-- /navbar-inner -->
   </div>
   <!-- /navbar -->
   <section>
      <?php
         if (!empty($data_peserta->row_array())) {
             $dp = $data_peserta->row_array();
             ?>
      <table class="table table-hover table-condensed">
         <thead>
            <tr>
               <th width="10%">
                  <div align="center">No.</div>
               </th>
               <th width="60%">Peserta</th>
               <th width="30%">
                  <div align="center">Action</div>
               </th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <td width="10%">
                  <div align="center">1.</div>
               </td>
               <td width="60%"><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                  <?php echo $dp['no_peserta']; ?><br />
                  <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br /><?php echo $dp['satker']; ?><br /><?php echo $dp['agama']; ?><br /><?php echo $dp['tempat_lahir_ktp']; ?>, <?php echo datedoank($dp['tgl_lahir_ktp']); ?> (<?php
                     $lahir = new DateTime($dp['tgl_lahir_ktp']);
                     $today = new DateTime();
                     $umur = $today->diff($lahir);
                     echo $umur->y;
                     echo " Tahun, ";
                     echo $umur->m;
                     echo " Bulan, dan ";
                     echo $umur->d;
                     echo " Hari";
                     ?>)<br /><?php echo $dp['lembaga_pendidikan']; ?>
               </td>
               <td width="30%">
                  <div align="center">
                     <a class="btn btn-small" href="<?php echo base_url(); ?>ujian/mulai_ujian/<?php echo encrypt_url($dp['no_peserta']); ?>"><i class="icon-pencil"></i> Mulai Ujian</a>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
      <?php
         }
         ?>
      <?php
                if (!empty($data_berkas->result_array())) {
                 ?>
      <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th><div align="center">No.</div></th>
                    <th>Peserta</th>
                    <th><div align="center">File</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    foreach ($data_berkas->result_array() as $dp) {
                        ?>
                        <tr>
                            <td><div align="center"><?php echo $no; ?>.</div></td>
                            <td><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                                <?php echo $dp['no_peserta']; ?><br />
                                <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br /><?php echo $dp['satker']; ?><br /><?php echo $dp['tempat_lahir_ktp']; ?>, <?php echo datedoank($dp['tgl_lahir_ktp']); ?> (<?php
                                $lahir = new DateTime($dp['tgl_lahir_ktp']);
                                $today = new DateTime();
                                $umur = $today->diff($lahir);
                                echo $umur->y;
                                echo " Tahun, ";
                                echo $umur->m;
                                echo " Bulan, dan ";
                                echo $umur->d;
                                echo " Hari";
                                ?>)<br /><?php echo $dp['lembaga_pendidikan']; ?>
                            </td>
                            <td>
                                <div align="center">
                                    <a href="<?php echo base_url(); ?>upload/ujian/<?php echo $dp['nama_berkas']; ?>">Download (<?php echo $dp['nama_berkas']; ?>)</a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                }
                ?>
            </tbody>
        </table>
      <?php	  
         if (empty($cek_ujian)) {
			 
         } else {
             $no_peserta = $this->session->userdata('username');
             echo "<tr><td colspan='3'><h4><div align='center'>Anda Sudah Ujian !!!<br/><br/><a class='btn btn-small' href='" . base_url() . "ujian/upload/" . encrypt_url($no_peserta) . "'><i class='icon-pencil'></i> Upload Ujian</a> </div></h4></td></tr>";
         }
         ?>
   </section>
</div>