<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?php echo base_url(); ?>samapta">Pelamar Ujian SKB - Tes Kesamaptaan</a>
                <div class="span6 pull-right">
                    <?php echo form_open("samapta/cari", 'class="navbar-form pull-right"'); ?>
                    <input type="text" class="span2" name="cari" placeholder="">
                    <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Pencarian</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <div align="center">
            <ul>
                <strong><?php echo $result_count; ?></strong>
            </ul>
        </div>
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th><div align="center">No.</div></th>
                    <th>Peserta</th>
                    <th><div align="center">Aksi</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data_peserta->result_array())) {
                    $no = $tot + 1;
                    foreach ($data_peserta->result_array() as $dp) {
                        ?>
                        <tr>
                            <td><div align="center"><?php echo $no; ?>.</div></td>
                            <td><?php echo $dp['nama_ktp']; ?> [<?php echo $dp['jenis_kelamin']; ?>]<br />
                                <?php echo $dp['no_peserta']; ?><br />
                                <?php echo $dp['jabatan_nama']; ?> [<?php echo $dp['jenis_formasi']; ?>]<br /><?php echo $dp['satker']; ?><br /><?php echo $dp['tempat_lahir_ktp']; ?>, <?php echo datedoank($dp['tgl_lahir_ktp']); ?> (<?php
                                $lahir = new DateTime($dp['tgl_lahir_ktp']);
                                $today = new DateTime();
                                $umur = $today->diff($lahir);
                                echo $umur->y;
                                echo " Tahun, ";
                                echo $umur->m;
                                echo " Bulan, dan ";
                                echo $umur->d;
                                echo " Hari";
                                ?>)<br /><?php echo $dp['lembaga_pendidikan']; ?>
                            </td>
                            <td>
        <?php
        if ($this->session->userdata('stts') != "koni_pusat") {
            ?>
                                    <div align="center">
                                        <a class="btn btn-small" href="<?php echo base_url(); ?>samapta/hadir/<?php echo encrypt_url($dp['id_peserta']); ?>"><i class="icon-pencil"></i> Hadir</a>
                                        <a class="btn btn-small" href="<?php echo base_url(); ?>samapta/tidak_hadir/<?php echo encrypt_url($dp['id_peserta']); ?>" onClick="return confirm('Anda yakin peserta ini tidak hadir tes keterampilan samapta ?');"><i class="icon-trash"></i> Tidak Hadir</a>
                                    </div>
            <?php
        } else {
            ?>
                                    <div align="center">
                                        <a class="btn btn-small" href="<?php echo base_url(); ?>samapta/hadir/<?php echo encrypt_url($dp['id_peserta']); ?>"><i class="icon-pencil"></i> Nilai</a>
                                        <a class="btn btn-small" href="<?php echo base_url(); ?>samapta/tidak_hadir_koni/<?php echo encrypt_url($dp['id_peserta']); ?>" onClick="return confirm('Anda yakin peserta ini tidak hadir tes keterampilan samapta ?');"><i class="icon-trash"></i> Tidak Hadir</a>
                                    </div>
                            <?php
                        }
                        ?>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "<tr><td colspan='3'><h4><div align='center'>Data Tidak ada !!!</div></h4></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="pagination pagination-centered">
            <ul>
<?php
echo $paginator;
?>
            </ul>
        </div>
    </section>
</div>
