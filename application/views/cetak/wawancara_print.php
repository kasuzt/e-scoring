<html>
    <head>
        <title>e-Scroring SKB CPNS Kejaksaan Republik Indonesia 1619</title>
        <style type="text/css">
            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

            th, td {
                text-align: left;
                padding: 16px;
            }

            tr:nth-child(even) {
                background-color: #f2f2f2;
            }
            .style1 {
                font-family: helvetica;
            }
        </style>
    </head>
    <body>
        <div align="center">
            <h2><strong><span class="style1">KEJAKSAAN AGUNG REPUBLIK INDONESIA<br>Panitia Penerimaan CPNS Kejaksaan Republik Indonesia 2021<br>
                        <br />Hasil Tes Wawancara <?php echo $nama_lengkap; ?><br/><?php echo $jabatan; ?></span></strong></h2>
            <p>&nbsp;</p>
        </div>
        <table border="1" align="center" width="100%">
            <tr>
                <th align="center"><span class="style1">No.</span></th>
                <th align="center"><span class="style1">Peserta</span></th>
                <th align="center"><span class="style1">Pengetahuan<br/>
                        Umum</span></th>
                <th align="center"><span class="style1">Pengetahuan<br/>
                        Akademik</span></th>
                <th align="center"><span class="style1">Motivasi<br/>
                        Bekerja</span></th>
                <th align="center"><span class="style1">Pengambilan<br/>
                        Keputusan</span></th>
                <th align="center"><span class="style1">Penampilan<br/>
                        Sikap</span></th>
                <th align="center"><span class="style1">Kemampuan<br/>
                        Verbal</span></th>
                <th align="center"><span class="style1">Keterangan</span></th>
                <th align="center"><span class="style1">Tanggal<br/>
                        Ujian</span></th>
            </tr>
            <?php
            if (!empty($data_peserta)) {
                $no = 1;
                foreach ($data_peserta->result() as $data) {
                    ?>
                    <tr>
                        <td><p align="center"><?php echo $no; ?>.</p></td>
                        <td>
                            <?php echo $data->nama_ktp; ?> [<?php echo $data->jenis_kelamin; ?>]<br/>
                            <?php echo $data->no_peserta; ?><br/>
                            <?php echo $data->jabatan_nama; ?><br/><?php echo $data->jenis_formasi; ?><br/><?php echo $data->satker; ?>                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->pengetahuan_umum != '') {
                                    echo $data->pengetahuan_umum;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->pengetahuan_akademik != '') {
                                    echo $data->pengetahuan_akademik;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->motivasi_bekerja != '') {
                                    echo $data->motivasi_bekerja;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->pengambilan_keputusan != '') {
                                    echo $data->pengambilan_keputusan;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->penampilan_sikap != '') {
                                    echo $data->penampilan_sikap;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->kemampuan_verbal != '') {
                                    echo $data->kemampuan_verbal;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>                        </td>
                        <td><p align="center"><?php echo $data->keterangan; ?></p></td>
                        <td><p align="center"><?php echo datetimes($data->created_time); ?></p></td>
                    </tr>
                    <?php
                    $no++;
                }
            }
            ?>
        </table>
        <br /><br /><strong><div align="center"><?php echo datedoank(date("Y/m/d")); ?><br /><?php echo $jabatan; ?>,<br /><br /><br /><br /><br /><br /><u><?php echo $nama_lengkap; ?></u><br />Nip. <?php echo $nip; ?></div></strong>
        <p>&nbsp;</p>
    </body>
</html>
