<html>
    <head>
        <title>e-Scroring SKB CPNS Kejaksaan Republik Indonesia 1619</title>
    </head>
    <style type="text/css">
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 16px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        body {
            font-family: verdana, arial, sans-serif;
        }
    </style>
    <body>
        <h1 style="text-align: center;">KEJAKSAAN AGUNG REPUBLIK INDONESIA<br>Panitia Penerimaan CPNS Kejaksaan Republik Indonesia 2021<br><br>Hasil Tes Keterampilan Beladiri <?php echo $nama_lengkap; ?></h1>
        <table border="1" align="center" width="100%">
            <tr>
                <th align="center">No.</th>
                <th align="center">Peserta</th>
                <th align="center">Warming Up</th>
                <th align="center">Kuda-Kuda</th>
                <th align="center">Tangkisan</th>
                <th align="center">Pukulan</th>
                <th align="center">Tendangan</th>
                <th align="center">Rangkaian<br/>Gerak/Kata</th>
                <th align="center">Keterangan</th>
                <th align="center">Tanggal Ujian</th>
            </tr>
            <?php
            if (!empty($data_peserta)) {
                $no = 1;
                foreach ($data_peserta->result() as $data) {
                    ?>
                    <tr>
                        <td><p align="center"><?php echo $no; ?>.</p></td>
                        <td>
                            <?php echo $data->nama_ktp; ?> [<?php echo $data->jenis_kelamin; ?>]<br/>
                            <?php echo $data->no_peserta; ?><br/>
                            <?php echo $data->jabatan_nama; ?> [<?php echo $data->jenis_formasi; ?>]<br/><?php echo $data->satker; ?>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->warming_up != '') {
                                    echo $data->warming_up;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->kuda_kuda != '') {
                                    echo $data->kuda_kuda;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->tangkisan != '') {
                                    echo $data->tangkisan;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->pukulan != '') {
                                    echo $data->pukulan;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->tendangan != '') {
                                    echo $data->tendangan;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->rangkaian_gerak != '') {
                                    echo $data->rangkaian_gerak;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td><p align="center"><?php echo $data->keterangan; ?></p></td>
                        <td><p align="center"><?php echo datetimes($data->created_time); ?></p></td>
                    </tr>
                    <?php
                    $no++;
                }
            }
            ?>
        </table>
    </body>
</html>
