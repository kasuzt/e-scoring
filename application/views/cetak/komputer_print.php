<html>
    <head>
        <title>e-Scroring SKB CPNS Kejaksaan Republik Indonesia 1619</title>
    </head>
    <style type="text/css">
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 16px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
    </style>
    <body>
        <?php $user = $this->input->post('user'); ?>
        <h1 style="text-align: center;">KEJAKSAAN AGUNG REPUBLIK INDONESIA<br>Panitia Penerimaan CPNS Kejaksaan Republik Indonesia 2021<br><br>Hasil Tes Keterampilan Komputer <?php echo $nama_lengkap; ?></h1>
        <table border="1" align="center" id="customers" width="100%">
            <tr>
                <th align="center">No.</th>
                <th align="center">Peserta</th>
                <th align="center">Kecepatan /<br/>Durasi</th>
                <th align="center">Ketelitian</th>
                <th align="center">Kerapian</th>
                <th align="center">Penguasaan<br/>Program</th>
                <th align="center">Keterangan</th>
                <th align="center">Tanggal<br/>Ujian</th>
            </tr>
            <?php
            if (!empty($data_peserta)) {
                $no = 1;
                foreach ($data_peserta->result() as $data) {
                    ?>
                    <tr>
                        <td><p align="center"><?php echo $no; ?>.</p></td>
                        <td>
                            <?php echo $data->nama_ktp; ?> [<?php echo $data->jenis_kelamin; ?>]<br/>
                            <?php echo $data->no_peserta; ?><br/>
                            <?php echo $data->jabatan_nama; ?> [<?php echo $data->jenis_formasi; ?>]<br/><?php echo $data->satker; ?>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->kecepatan_durasi != '') {
                                    echo $data->kecepatan_durasi;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->ketelitian != '') {
                                    echo $data->ketelitian;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->kerapian != '') {
                                    echo $data->kerapian;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td>
                            <p align="center">
                                <?php
                                if ($data->penguasaan_program != '') {
                                    echo $data->penguasaan_program;
                                } else {
                                    echo '<p align="center">0</p>';
                                }
                                ?>
                            </p>
                        </td>
                        <td><p align="center"><?php echo $data->keterangan; ?></p></td>
                        <td><p align="center"><?php echo datetimes($data->created_time); ?></p></td>
                    </tr>
                    <?php
                    $no++;
                }
            }
            ?>
        </table>

    </body>
</html>
