<div class="well">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?php echo base_url(); ?>cetak">Cetak Hasil Tes SKB</a>
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <section>
        <legend>Hasil Tes Wawancara</legend>
        <form action="<?php echo base_url(); ?>cetak/wawancara" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div class="control-group">
                <div class="span2"><strong>Pilih Pewawancara</strong></div>
                <div class="span">:</div>
                <div class="span6" id="user">
                    <select class="form-control span5" name="user">
                        <?php
                        foreach ($user_wawancara as $row) {
                            echo '<option value="' . $row->username . '">' . $row->nama_lengkap . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary" formtarget="_blank">Cetak</button>
                </div>
            </div>
        </form>

        <legend>Hasil Tes Komputer</legend>
        <form action="<?php echo base_url(); ?>cetak/komputer" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div class="control-group">
                <div class="span2"><strong>Pilih Penilai</strong></div>
                <div class="span">:</div>
                <div class="span6" id="user">
                    <select class="form-control" name="user">
                        <?php
                        foreach ($user as $row) {
                            echo '<option value="' . $row->username . '">' . $row->nama_lengkap . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary" formtarget="_blank">Cetak</button>
                </div>
            </div>
        </form>

        <legend>Hasil Tes Beladiri</legend>
        <form action="<?php echo base_url(); ?>cetak/beladiri" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div class="control-group">
                <div class="span2"><strong>Pilih Penilai</strong></div>
                <div class="span">:</div>
                <div class="span6" id="user">
                    <select class="form-control" name="user">
                        <?php
                        foreach ($user as $row) {
                            echo '<option value="' . $row->username . '">' . $row->nama_lengkap . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary" formtarget="_blank">Cetak</button>
                </div>
            </div>
        </form>

        <legend>Hasil Tes Mengemudi</legend>
        <form action="<?php echo base_url(); ?>cetak/mengemudi" class="form-horizontal" id="formID" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div class="control-group">
                <div class="span2"><strong>Pilih Penilai</strong></div>
                <div class="span">:</div>
                <div class="span6" id="user">
                    <select class="form-control" name="user">
                        <?php
                        foreach ($user as $row) {
                            echo '<option value="' . $row->username . '">' . $row->nama_lengkap . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary" formtarget="_blank">Cetak</button>
                </div>
            </div>
        </form>
    </section>
</div>
