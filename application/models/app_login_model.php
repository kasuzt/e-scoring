<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App_Login_Model extends CI_Model {

    public function getLoginData($data) {
        $login['username'] = $data['username'];
        $login['password'] = md5($data['password'] . 'AppSimpeg32');
        // $hash = hash_pbkdf2("sha256", $data['password'], MSK, 100);
        // $login['password'] = $hash;
        $cek = $this->db->get_where('tbl_user_login', $login);
        if ($cek->num_rows() > 0) {
            foreach ($cek->result() as $qad) {
                $sess_data['logged_in'] = 'yesGetMeLoginBaby';
                $sess_data['id_user'] = $qad->id_user_login;
                $sess_data['username'] = $qad->username;
                $sess_data['nama'] = $qad->nama_lengkap;
                $sess_data['kode_satker'] = $qad->kode_satker;
                $sess_data['stts'] = $qad->stts;
                $sess_data['id_master_ujian'] = $qad->id_master_ujian;
                $this->session->set_userdata($sess_data);
            }
            header('location:' . base_url() . '');
        } else {
            $this->session->set_flashdata('result_login', "Maaf, kombinasi username dan password yang anda masukkan tidak valid dengan database kami.");
            header('location:' . base_url() . '');
        }
    }

    function getAllUser() {
        $query = $this->db->query('SELECT id_user_login, username, nama_lengkap FROM tbl_user_login WHERE id_user_login < 33');
        return $query->result();
    }

    function getAllUserPewawancara() {
        $query = $this->db->query('SELECT id_user_login, username, nama_lengkap FROM tbl_user_login WHERE stts = "pewawancara"');
        return $query->result();
    }

    function getCetakWawancara($user) {
        $query = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_wawancara.* FROM tbl_data_peserta, tbl_nilai_wawancara WHERE tbl_data_peserta.id_peserta = tbl_nilai_wawancara.id_peserta AND tbl_nilai_wawancara.penguji = '$user' GROUP BY tbl_data_peserta.id_peserta ORDER BY created_time");
        return $query;
    }

    function getCetakKomputer($user) {
        $query = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_komputer.*, tbl_user_login.* FROM tbl_data_peserta, tbl_nilai_komputer, tbl_user_login WHERE tbl_data_peserta.id_peserta = tbl_nilai_komputer.id_peserta AND tbl_nilai_komputer.penguji = '$user' GROUP BY tbl_data_peserta.id_peserta ORDER BY created_time");
        return $query;
    }

    function getCetakBeladiri($user) {
        $query = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_beladiri.*, tbl_user_login.* FROM tbl_data_peserta, tbl_nilai_beladiri, tbl_user_login WHERE tbl_data_peserta.id_peserta = tbl_nilai_beladiri.id_peserta AND tbl_nilai_beladiri.penguji = '$user' GROUP BY tbl_data_peserta.id_peserta ORDER BY created_time");
        return $query;
    }

    function getCetakMengemudi($user) {
        $query = $this->db->query("SELECT tbl_data_peserta.*, tbl_nilai_mengemudi.*, tbl_user_login.* FROM tbl_data_peserta, tbl_nilai_mengemudi, tbl_user_login WHERE tbl_data_peserta.id_peserta = tbl_nilai_mengemudi.id_peserta AND tbl_nilai_mengemudi.penguji = '$user' GROUP BY tbl_data_peserta.id_peserta ORDER BY created_time");
        return $query;
    }

    function ubahBeladiri($id_peserta, $tgl_beladiri) {
        $hasil = $this->db->query("UPDATE tbl_data_peserta SET tgl_beladiri='$tgl_beladiri' WHERE id_peserta='$id_peserta'");
        // return $hsl;
    }

    function ubahMengemudi($id_peserta, $tgl_mengemudi) {
        $hasil = $this->db->query("UPDATE tbl_data_peserta SET tgl_mengemudi='$tgl_mengemudi' WHERE id_peserta='$id_peserta'");
        // return $hsl;
    }

    function ubahKomputer($id_peserta, $tgl_komputer) {
        $hasil = $this->db->query("UPDATE tbl_data_peserta SET tgl_komputer='$tgl_komputer' WHERE id_peserta='$id_peserta'");
        // return $hsl;
    }

    function ubahWawancara($id_peserta, $tgl_wawancara, $id_user) {
        $hasil = $this->db->query("UPDATE tbl_data_peserta SET tgl_wawancara='$tgl_wawancara', id_user='$id_user' WHERE id_peserta='$id_peserta'");
        // return $hsl;
    }

    function getCetakAll($kode_satker) {
        $query = $this->db->query("SELECT * FROM tbl_data_peserta WHERE kode_satker = '$kode_satker' AND pendidikan_nama = 'SLTA/SMA SEDERAJAT' ORDER BY jabatan_nama, nama_ktp");
        return $query;
    }

    function getCetakAllAdmin() {
        $query = $this->db->query("SELECT * FROM tbl_data_peserta WHERE pendidikan_nama = 'SLTA/SMA SEDERAJAT'");
        return $query;
    }

    function getCetakPewawancara($user) {
        $query = $this->db->query("SELECT tbl_data_peserta.*, tbl_user_login.* FROM tbl_data_peserta, tbl_user_login WHERE tbl_data_peserta.id_user ='$user' GROUP BY tbl_data_peserta.id_peserta ORDER BY tgl_wawancara, nama_ktp");
        return $query;
    }

}

/* End of file app_login_model.php */
/* Location: ./application/models/app_login_model.php */