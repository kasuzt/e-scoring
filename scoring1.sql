/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : scoring

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 30/11/2022 15:16:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_berkas
-- ----------------------------
DROP TABLE IF EXISTS `tbl_berkas`;
CREATE TABLE `tbl_berkas`  (
  `kd_berkas` int(11) NOT NULL AUTO_INCREMENT,
  `no_peserta` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_berkas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan_berkas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipe_berkas` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ukuran_berkas` float NULL DEFAULT NULL,
  `waktu_upload` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`kd_berkas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_berkas
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_data_peserta
-- ----------------------------
DROP TABLE IF EXISTS `tbl_data_peserta`;
CREATE TABLE `tbl_data_peserta`  (
  `id_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_register` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_peserta` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_daftar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_ktp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tempat_lahir_ktp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_lahir_ktp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gelar_depan_ijazah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_ijazah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gelar_belakang_ijazah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tempat_lahir_ijazah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_lahir_ijazah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_kelamin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `agama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_disabilitas` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `link_disabilitas` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat_domisili` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kabkota_domisili` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `provinsi_domisili` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lembaga_pendidikan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_ijazah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_ijazah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tahun_lulus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `akreditasi_lembaga` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `akreditasi_prodi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ipk_nilai` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jabatan_kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jabatan_nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lokasi_kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lokasi_nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_formasi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pendidikan_kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pendidikan_nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lokasi_ujian_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lokasi_ujian_nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pt_dikti` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `prodi_dikti` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_verifikasi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `verifikator_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `verifikator_nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `supervisor_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `supervisor_nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_supervisi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lokasi_ujian_skb_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lokasi_ujian_skb_nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `satker` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kode_satker` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_user` int(11) NULL DEFAULT NULL,
  `tgl_wawancara` date NULL DEFAULT NULL,
  `tgl_praktek_kerja` date NULL DEFAULT NULL,
  `tgl_beladiri` date NULL DEFAULT NULL,
  `tgl_samapta` date NULL DEFAULT NULL,
  `id_praktek_kerja` int(11) NULL DEFAULT NULL,
  `tgl_presentasi` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_peserta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_data_peserta
-- ----------------------------
INSERT INTO `tbl_data_peserta` VALUES (1, '196707031992031003', '196707031992031003', '196707031992031003', NULL, 'LEONARD E E SIMANJUNTAK, S.H., M.H.', 'TAPANULI', '1967-07-03', NULL, 'LEONARD E E SIMANJUNTAK, S.H., M.H.', NULL, NULL, '03/07/1967', 'Laki-Laki', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KEPALA KEJAKSAAN TINGGI BANTEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-11-30');
INSERT INTO `tbl_data_peserta` VALUES (2, '196808271991031004', '196808271991031004', '196808271991031004', NULL, 'AGUS SALIM, S.H., M.H.', 'PALOPO', '1968-08-27', NULL, 'AGUS SALIM, S.H., M.H.', NULL, NULL, NULL, 'Laki-Laki', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KEPALA KEJAKSAAN TINGGI SULAWESI TENGAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-11-30');
INSERT INTO `tbl_data_peserta` VALUES (3, '196901291994032006', '196901291994032006', '196901291994032006', NULL, 'KATARINA ENDANG SARWESTRI, S.H., M.H.', 'SAWIT BOYOLALI', '1994-03-01', NULL, 'KATARINA ENDANG SARWESTRI, S.H., M.H.', NULL, NULL, NULL, 'Perempuan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KEPALA KEJAKSAAN TINGGI DI YOGYAKARTA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-11-30');
INSERT INTO `tbl_data_peserta` VALUES (4, '196811071994031004', '196811071994031004', '196811071994031004', NULL, 'IMAN WIJAYA, S.H., M.Hum.', 'PALEMBANG', '1968-11-07', NULL, 'IMAN WIJAYA, S.H., M.Hum.', NULL, NULL, NULL, 'Laki-Laki', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'DIREKTUR EKONOMI DAN KEUANGAN PADA JAM INTELIJEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-11-30');
INSERT INTO `tbl_data_peserta` VALUES (5, '196907221994031001', '196907221994031001', '196907221994031001', NULL, 'Dr. YULIANTO, S.H., M.H.', 'BANYUWANGI', '1969-07-22', NULL, 'Dr. YULIANTO, S.H., M.H.', NULL, NULL, NULL, 'Laki-Laki', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KEPALA PUSAT PENDIDIKAN DAN PELATIHAN TEKNIS FUNGSIONAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-11-30');
INSERT INTO `tbl_data_peserta` VALUES (6, '197104281995031001', '197104281995031001', '197104281995031001', NULL, 'Dr. SUPARDI, S.H., M.H.', 'BOYOLALI', '1971-04-28', NULL, 'Dr. SUPARDI, S.H., M.H.', NULL, NULL, NULL, 'Laki-Laki', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KEPALA KEJAKSAAN TINGGI RIAU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-11-30');

-- ----------------------------
-- Table structure for tbl_master_ujian
-- ----------------------------
DROP TABLE IF EXISTS `tbl_master_ujian`;
CREATE TABLE `tbl_master_ujian`  (
  `id_master_ujian` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_master_ujian`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_master_ujian
-- ----------------------------
INSERT INTO `tbl_master_ujian` VALUES (1, 'SOAL 1', 'SOAL 1', '07d2cc40ae492de70e18c476912e9ab0.pdf', '2021-11-16 13:47:37', '2021-11-17 00:02:45');
INSERT INTO `tbl_master_ujian` VALUES (2, 'SOAL 2', 'SOAL 2', '328314ffbee53f687d4fd5d041d2a0e2.pdf', '2021-11-16 13:47:49', NULL);
INSERT INTO `tbl_master_ujian` VALUES (3, 'SOAL 3', 'SOAL 3', 'a5151e9ee2e82868017646afc1e0c3e6.pdf', '2021-11-16 13:54:22', NULL);
INSERT INTO `tbl_master_ujian` VALUES (4, 'SOAL 4', 'SOAL 4', '575c1bafa9d3a5d08330b992ada73add.pdf', '2021-11-16 13:54:40', NULL);

-- ----------------------------
-- Table structure for tbl_nilai_beladiri
-- ----------------------------
DROP TABLE IF EXISTS `tbl_nilai_beladiri`;
CREATE TABLE `tbl_nilai_beladiri`  (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `kuda_kuda` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tangkisan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pukulan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tendangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rangkaian_gerak` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `penguji` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_nilai`) USING BTREE,
  UNIQUE INDEX `id_peserta`(`id_peserta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_nilai_beladiri
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_nilai_beladiri_koni
-- ----------------------------
DROP TABLE IF EXISTS `tbl_nilai_beladiri_koni`;
CREATE TABLE `tbl_nilai_beladiri_koni`  (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `kuda_kuda` int(11) NULL DEFAULT NULL,
  `tangkisan` int(11) NULL DEFAULT NULL,
  `pukulan` int(11) NULL DEFAULT NULL,
  `tendangan` int(11) NULL DEFAULT NULL,
  `rangkaian_gerak` int(11) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `penguji` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_nilai`) USING BTREE,
  UNIQUE INDEX `id_peserta`(`id_peserta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_nilai_beladiri_koni
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_nilai_praktek_kerja
-- ----------------------------
DROP TABLE IF EXISTS `tbl_nilai_praktek_kerja`;
CREATE TABLE `tbl_nilai_praktek_kerja`  (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `kecepatan_durasi` int(11) NULL DEFAULT NULL,
  `ketelitian` int(11) NULL DEFAULT NULL,
  `kerapian` int(11) NULL DEFAULT NULL,
  `penguasaan_program` int(11) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `penguji` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_nilai`) USING BTREE,
  UNIQUE INDEX `id_peserta`(`id_peserta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_nilai_praktek_kerja
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_nilai_presentasi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_nilai_presentasi`;
CREATE TABLE `tbl_nilai_presentasi`  (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `pengetahuan_umum` int(11) NULL DEFAULT NULL,
  `pengetahuan_akademik` int(11) NULL DEFAULT NULL,
  `motivasi_bekerja` int(11) NULL DEFAULT NULL,
  `pengambilan_keputusan` int(11) NULL DEFAULT NULL,
  `penampilan_sikap` int(11) NULL DEFAULT NULL,
  `kemampuan_verbal` int(11) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `penguji` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_nilai`) USING BTREE,
  UNIQUE INDEX `id_peserta`(`id_peserta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_nilai_presentasi
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_nilai_samapta
-- ----------------------------
DROP TABLE IF EXISTS `tbl_nilai_samapta`;
CREATE TABLE `tbl_nilai_samapta`  (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `lari` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pull_ups` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sit_ups` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `push_ups` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `shuttle_run` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `penguji` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_nilai`) USING BTREE,
  UNIQUE INDEX `id_peserta`(`id_peserta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_nilai_samapta
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_nilai_samapta_koni
-- ----------------------------
DROP TABLE IF EXISTS `tbl_nilai_samapta_koni`;
CREATE TABLE `tbl_nilai_samapta_koni`  (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `lari` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pull_ups` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sit_ups` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `push_ups` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `shuttle_run` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  `penguji` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id_nilai`) USING BTREE,
  UNIQUE INDEX `id_peserta`(`id_peserta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_nilai_samapta_koni
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_nilai_wawancara
-- ----------------------------
DROP TABLE IF EXISTS `tbl_nilai_wawancara`;
CREATE TABLE `tbl_nilai_wawancara`  (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `pengetahuan_umum` int(11) NULL DEFAULT NULL,
  `pengetahuan_akademik` int(11) NULL DEFAULT NULL,
  `motivasi_bekerja` int(11) NULL DEFAULT NULL,
  `pengambilan_keputusan` int(11) NULL DEFAULT NULL,
  `penampilan_sikap` int(11) NULL DEFAULT NULL,
  `kemampuan_verbal` int(11) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `penguji` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_nilai`) USING BTREE,
  UNIQUE INDEX `id_peserta`(`id_peserta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_nilai_wawancara
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_satker
-- ----------------------------
DROP TABLE IF EXISTS `tbl_satker`;
CREATE TABLE `tbl_satker`  (
  `id_satker` int(11) NOT NULL AUTO_INCREMENT,
  `kode_satker` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `satker` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_satker`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_satker
-- ----------------------------
INSERT INTO `tbl_satker` VALUES (1, '1', 'KEJAKSAAN TINGGI ACEH');
INSERT INTO `tbl_satker` VALUES (2, '2', 'KEJAKSAAN TINGGI SUMATERA UTARA');
INSERT INTO `tbl_satker` VALUES (3, '3', 'KEJAKSAAN TINGGI RIAU');
INSERT INTO `tbl_satker` VALUES (4, '4', 'KEJAKSAAN TINGGI SUMATERA BARAT');
INSERT INTO `tbl_satker` VALUES (5, '5', 'KEJAKSAAN TINGGI JAMBI');
INSERT INTO `tbl_satker` VALUES (6, '6', 'KEJAKSAAN TINGGI SUMATERA SELATAN');
INSERT INTO `tbl_satker` VALUES (7, '7', 'KEJAKSAAN TINGGI LAMPUNG');
INSERT INTO `tbl_satker` VALUES (8, '8', 'KEJAKSAAN TINGGI BENGKULU');
INSERT INTO `tbl_satker` VALUES (9, '10', 'KEJAKSAAN TINGGI JAWA BARAT');
INSERT INTO `tbl_satker` VALUES (10, '11', 'KEJAKSAAN TINGGI DI YOGYAKARTA');
INSERT INTO `tbl_satker` VALUES (11, '12', 'KEJAKSAAN TINGGI JAWA TENGAH');
INSERT INTO `tbl_satker` VALUES (12, '13', 'KEJAKSAAN TINGGI JAWA TIMUR');
INSERT INTO `tbl_satker` VALUES (13, '14', 'KEJAKSAAN TINGGI KALIMANTAN BARAT');
INSERT INTO `tbl_satker` VALUES (14, '15', 'KEJAKSAAN TINGGI KALIMANTAN TENGAH');
INSERT INTO `tbl_satker` VALUES (15, '16', 'KEJAKSAAN TINGGI KALIMANTAN SELATAN');
INSERT INTO `tbl_satker` VALUES (16, '17', 'KEJAKSAAN TINGGI KALIMANTAN TIMUR');
INSERT INTO `tbl_satker` VALUES (17, '18', 'KEJAKSAAN TINGGI SULAWESI UTARA');
INSERT INTO `tbl_satker` VALUES (18, '19', 'KEJAKSAAN TINGGI SULAWESI TENGGARA');
INSERT INTO `tbl_satker` VALUES (19, '20', 'KEJAKSAAN TINGGI SULAWESI TENGAH');
INSERT INTO `tbl_satker` VALUES (20, '21', 'KEJAKSAAN TINGGI SULAWESI SELATAN');
INSERT INTO `tbl_satker` VALUES (21, '22', 'KEJAKSAAN TINGGI BALI');
INSERT INTO `tbl_satker` VALUES (22, '23', 'KEJAKSAAN TINGGI NUSA TENGGARA BARAT');
INSERT INTO `tbl_satker` VALUES (23, '24', 'KEJAKSAAN TINGGI NUSA TENGGARA TIMUR');
INSERT INTO `tbl_satker` VALUES (24, '25', 'KEJAKSAAN TINGGI MALUKU');
INSERT INTO `tbl_satker` VALUES (25, '26', 'KEJAKSAAN TINGGI PAPUA');
INSERT INTO `tbl_satker` VALUES (26, '27', 'KEJAKSAAN TINGGI MALUKU UTARA');
INSERT INTO `tbl_satker` VALUES (27, '28', 'KEJAKSAAN TINGGI BANTEN');
INSERT INTO `tbl_satker` VALUES (28, '29', 'KEJAKSAAN TINGGI KEPULAUAN BANGKA BELITUNG');
INSERT INTO `tbl_satker` VALUES (29, '30', 'KEJAKSAAN TINGGI GORONTALO');
INSERT INTO `tbl_satker` VALUES (30, '31', 'KEJAKSAAN TINGGI KEPULAUAN RIAU');
INSERT INTO `tbl_satker` VALUES (31, '32', 'KEJAKSAAN TINGGI SULAWESI BARAT');
INSERT INTO `tbl_satker` VALUES (32, '33', 'KEJAKSAAN TINGGI PAPUA BARAT');
INSERT INTO `tbl_satker` VALUES (33, '00', 'KEJAKSAAN AGUNG');

-- ----------------------------
-- Table structure for tbl_ujian
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ujian`;
CREATE TABLE `tbl_ujian`  (
  `id_ujian` int(11) NOT NULL AUTO_INCREMENT,
  `no_peserta` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_master_ujian` int(11) NULL DEFAULT NULL,
  `waktu_mulai` datetime NULL DEFAULT NULL,
  `waktu_selesai` datetime NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_ujian`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_ujian
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_user_login
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_login`;
CREATE TABLE `tbl_user_login`  (
  `id_user_login` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_lengkap` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nip` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_master_ujian` int(11) NULL DEFAULT NULL,
  `stts` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_satker` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_user_login`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tbl_user_login
-- ----------------------------
INSERT INTO `tbl_user_login` VALUES (1, 'd35yk4', 'c1b7477bcab92dc932a428e36a631042', 'DESYKA PRIHANTARA', '198812082014031003', 'PERANCANG SISTEM INFORMASI KEPEGAWAIAN', NULL, 'administrator', '00');
INSERT INTO `tbl_user_login` VALUES (2, 'penguji1', '0752f06e45f941a7cf1adabe465d11fc', 'PENGUJI 1', NULL, NULL, NULL, 'presentasi', '00');

SET FOREIGN_KEY_CHECKS = 1;
